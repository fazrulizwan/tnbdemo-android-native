var module = angular.module('my-app', ['onsen']);
  module
  .controller('SplitterController', function($scope, $http) {
  this.load = function(page) {
    mySplitter.content.load(page)
      .then(function() {
        //console.log(page);
        mySplitter.left.close();
      });
  };



  //ons.bootstrap();
  $scope.app = {};

  ons.ready(function () {
    ons.createElement('action-sheet.html', { append: true })
      .then(function (sheet) {
        $scope.app.showFromTemplate = sheet.show.bind(sheet);
        $scope.app.hideFromTemplate = sheet.hide.bind(sheet);
      });
      ons.createElement('action-sheet-prog.html', { append: true })
        .then(function (sheet) {
          $scope.app.showFromTemplate = sheetProg.show.bind(sheet);
          $scope.app.hideFromTemplate = sheetProg.hide.bind(sheet);
        });
  });


$scope.onshow = function(whichPage){
  console.log("onshow:"+whichPage);
  if(whichPage == "tab1list"){
    $scope.$root.successWording = "Success disconnection";
    $scope.$root.pendingWording = "Pending disconnection";
    $scope.pageStackRecon = "";
    $scope.pageStack = "tab1list";
    $scope.loadAssignedJobList();
    $scope.getListCount();
  }
  else if(whichPage == "tab1detail"){
    $scope.pageStack = "tab1detail";
  }
  else if(whichPage == "tab2list"){
    $scope.pageStack = "tab2list";
    $scope.loadPendingJobList();
    $scope.getListCount();
  }
  else if(whichPage == "tab2detail"){
    $scope.pageStack = "tab2detail";
    $scope.loadJobImages($scope.curJobId);
  }
  else if(whichPage == "tab3list"){
    $scope.pageStack = "tab3list";
    $scope.loadCompleteJobList();
    $scope.getListCount();
  }
  else if(whichPage == "tab3detail"){
    $scope.pageStack = "tab3detail";
    $scope.loadJobImages($scope.curJobId);
  }
}


$scope.onhide = function(whichPage){
  if(whichPage == "tab1detail" && $scope.pageStack == whichPage){
    myNavigator.popPage( { animation : 'none'} );
  }
  else if(whichPage == "tab2detail" && $scope.pageStack == whichPage){
    myNavigator2.popPage( { animation : 'none'} );
  }
  else if(whichPage == "tab3detail" && $scope.pageStack == whichPage){
    myNavigator3.popPage( { animation : 'none'} );
  }
}


  $scope.doTakeGallery = function(){
    //console.log("dlo:"+curImgIndex);
    $scope.getGalleryImage();
    imageSheet.hide();
  }


  this.init = function(e) {
    // Ensure the emitter is the current page, not a nested one
    if (e.target === e.currentTarget) {
      var page = e.target;
      // Safely access data
      //console.log('init event', page.data);
      console.log("init at:"+page);
    }
  };


  this.onPopPost = function(event) {
    console.log("sad");
    console.log(event);

  };



  $scope.assJobs = {};
  $scope.pendJobs = {};
  $scope.compJobs = {};
  $scope.curImages = [];
  $scope.showLoader = false;
  $scope.pageStack = "";
  $scope.pageStackRecon = "";
  $scope.reason = {text: ""};
  $scope.reasonRecon = {text: ""};
  $scope.disconentry ={};

  //$scope.showMOD = false;

  $scope.title = 'Tab 1';
  $scope.updateTitle = function($event) {
    $scope.title = angular.element($event.tabItem).attr('label');
  };


  $scope.clickOKMOD = function(){
    $scope.showMOD = false;
    if(document.getElementById("r1").checked)
      $scope.selectedMOD = "fuse";
    else if(document.getElementById("r2").checked)
      $scope.selectedMOD = "meter";
    else if(document.getElementById("r3").checked)
      $scope.selectedMOD = "wire";
    console.log($scope.selectedMOD);
    ons.notification.confirm({message: 'Please confirm to update success disconnection & print disconnection letter?'})
    .then(function(answer) {
      if(answer==1){
        $scope.submitDisconSuccess();
        var img1 = "";
        var img2 = "";
        var imgCount = 0;
        var printImg = false;
        if(curImages[0] != "null" && curImages[0] != ""){
          img1 = curImages[0];
          imgCount++;
        }
        if(curImages[1] != "null" && curImages[1] != ""){
          img2 = curImages[1];
          imgCount++;
        }
        if(imgCount > 0) printImg = true;

        if(TARGET == "device")
          showAndroidToast($scope.getCurData().customer, $scope.getCurData().addrs1, $scope.getCurData().addrs2, $scope.getCurData().addrs3, $scope.getCurData().acctno, $scope.getCurData().sono, $scope.getCurData().ar_ob, $scope.getCurData().ar_ad, $scope.getCurData().ar_rd, img1, img2, printImg, imgCount);
        $scope.infoDlgTitle = "Submitted";
        $scope.infoDlgBody = "Progress submitted & disconnection notice sent for printing";
        $scope.infoDlgShow = true;
      }
    });
  }



  $scope.clickOKReason = function(){
    $scope.showReason = false;
    if(document.getElementById("reason1").checked)
      $scope.selectedReason = "reason1";
    else if(document.getElementById("reason2").checked)
      $scope.selectedReason = "reason2";
    else if(document.getElementById("reason3").checked)
      $scope.selectedReason = "reason3";
    else if(document.getElementById("reason99").checked){
      $scope.selectedReason = "reason99"
      //$scope.reasonTxt = document.getElementById("reasontext");
    }
    console.log("reaon text:"+$scope.reason.text);
    ons.notification.confirm({message: 'Please confirm to update pending disconnection & print disconnection pending letter?'})
    .then(function(answer) {
      if(answer==1){
        $scope.submitDisconPending();
        var img1 = "";
        var img2 = "";
        var imgCount = 0;
        var printImg = false;
        if(curImages[0] != "null" && curImages[0] != ""){
          img1 = curImages[0];
          imgCount++;
        }
        if(curImages[1] != "null" && curImages[1] != ""){
          img2 = curImages[1];
          imgCount++;
        }
        if(imgCount > 0) printImg = true;
        if(TARGET == "device")
          printDisconFail($scope.getCurData().customer, $scope.getCurData().addrs1, $scope.getCurData().addrs2, $scope.getCurData().addrs3, $scope.getCurData().acctno, $scope.getCurData().sono, $scope.getCurData().ar_ob, $scope.getCurData().ar_ad, $scope.getCurData().ar_rd, img1, img2, printImg, imgCount);
        $scope.infoDlgTitle = "Submitted";
        $scope.infoDlgBody = "Progress submitted & disconnection pending notice sent for printing";
        $scope.infoDlgShow = true;
      }
    });

  }


  $scope.getAllCount = function(){
    var assCounter = 0;
    var pendCounter = 0;
    var compCounter = 0;

    if($scope.getAssCount() === undefined)
      assCounter = 0;
    else
      assCounter = $scope.getAssCount();

    if($scope.getPendingCount() === undefined)
      pendCounter = 0;
    else
      pendCounter = $scope.getPendingCount();

    if($scope.getCompleteCount() === undefined)
      compCounter = 0;
    else
      compCounter = $scope.getCompleteCount();

    if(assCounter + pendCounter + compCounter == 0)
      return;
    else
      return assCounter + pendCounter + compCounter;
  }

  $scope.getAssCount = function(){
    if($scope.listCount === undefined) return;
    if($scope.listCount[0] > 0)
      return $scope.listCount[0];
  }

  $scope.getPendingCount = function(){
    //console.log("count 2:"+$scope.listCount[1]);
    if($scope.listCount === undefined) return;
    if($scope.listCount[1] > 0)
      return $scope.listCount[1];
  }

  $scope.getCompleteCount = function(){
    if($scope.listCount === undefined) return;
    if($scope.listCount[2] > 0)
      return $scope.listCount[2];
  }


  $scope.clickCancelMOD = function(){
    $scope.showMOD = false;
  }


  $scope.clickCancelReason = function(){
    $scope.showReason = false;
  }


  $scope.showAlert = function(message, btn) {
    $scope.toastMsg = message;
    $scope.toastBtn = btn;

    toast.toggle();
	  showAndroidToast("Mr bean!");
  };



	$scope.setTitle = function(txt){
	//console.log("dlm ang "+txt);
		$scope.testpow = txt;
	}



  $scope.goTo =  function(where){
    if(where == "discon")
      mySplitter.content.load("home.html");
    else if(where == "recon")
      mySplitter.content.load("settings.html");
  }


  $scope.backTab1Detail = function () {
    console.log($scope.pageStack);
    if($scope.pageStack == "tab1detail"){
      $scope.infoDlgShow = false;
      myNavigator.popPage( { animation : 'none'} );
    }
  };



  $scope.getCurData = function(){
    //console.log("pgstack:"+$scope.pageStack);
    if($scope.pageStack == "tab1detail"){
      for(i=0; i<$scope.assJobs.length; i++){
          if($scope.assJobs[i].id == $scope.curJobId){
            //console.log($scope.assJobs[i].id);
            return $scope.assJobs[i];
          }
      }
    }
    else if($scope.pageStack == "tab2detail"){
      for(i=0; i<$scope.pendJobs.length; i++){
          if($scope.pendJobs[i].id == $scope.curJobId){
            //console.log($scope.pendJobs[i]);
            return $scope.pendJobs[i];
          }
      }
    }
    else if($scope.pageStack == "tab3detail"){
      for(i=0; i<$scope.compJobs.length; i++){
          if($scope.compJobs[i].id == $scope.curJobId){
            //console.log($scope.compJobs[i]);
            return $scope.compJobs[i];
          }
      }
    }
  }



  $scope.getCameraImage = function(){
    androidGetImageCamera();
  }


  $scope.getGalleryImage = function(){
    androidGetImageGallery();
  }



  $scope.doPushPage = function(page, jobId){
    if(page == "tab1detail"){
      $scope.showMOD = false;
      $scope.disconentry = {};
      curImages = ["null", "null"];
      $scope.curJobId = jobId;
      myNavigator.pushPage('tab1detail.html', {data: {title: 'Page 2'}});
    }
    else if(page == "tab2detail"){
      curImages = ["null", "null"];
      $scope.curJobId = jobId;
      myNavigator2.pushPage('tab2detail.html', {data: {title: 'Page 2'}});
    }
    else if(page == "tab3detail"){
      curImages = ["null", "null"];
      $scope.curJobId = jobId;
      myNavigator3.pushPage('tab3detail.html', {data: {title: 'Page 2'}});
    }
  }



  $scope.imageUpload = function(event){
       var files = event.target.files; //FileList object

       for (var i = 0; i < files.length; i++) {
           var file = files[i];
           var reader = new FileReader();
           reader.onload = $scope.imageIsLoaded;
           reader.readAsDataURL(file);
       }
  }

  $scope.imageIsLoaded = function(e){
      $scope.$apply(function() {
          $scope.uploadImg = e.target.result;
          //console.log($scope.uploadImg);
      });
  }



  $scope.getImageCaptured = function(img){
      curImages[curImgIndex] = img;
      $scope.$apply();
      //console.log("curimage:"+curImages[curImgIndex]);
      //console.log(curImages);
  }



  $scope.getTN = function(index) {
    //console.log("curimage in tn:"+curImages[index]);
    //console.log(curImages);
    if(curImages[index] == "null" || curImages[index] == "")
      return "iVBORw0KGgoAAAANSUhEUgAAAHgAAAB4CAYAAAA5ZDbSAAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAAJOjAACTowHRsvDGAAAAB3RJTUUH4ggODBQ6pk/bWAAADutJREFUeNrtnWtsHNd1x3/3zuzM7C73QVIPR00CWXbtlIqDoB/8wUALmVyLRoC6SWsgX4qaFhLUSNq4gKmgSJMadR0UregaTpCmsetq7RRp7bZG4LR1ViEZxWmNJFYiJbZk2YpFyRFViSIpkuJrZ3fm9sPuUrRM7pP7ou4fWJCUdmZn7u+ec885995Z0NLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0qpcB1LHeOInk7oh6qQnfjLJgdSxms4hqj1waPgEg4meVX8flyBkLefUAkCB8gcTu/312rqugB8bPs5Did2FD5ZAPP+6BdgBRHMXqVUljzngPPAWMAPMDCZ6/Gvbvi6Ah4aPM3gV7jbgDuCzQEKzqYuGga8BrwwmeiauZbChgK+x3A8DXwbu0QwaoheBPx9M9LxeqSXLcj/Bz791aPjEHcALGm5DdQ/wQr7tV1hsmAUXBvmh4RO7gG8Dt+k2b4peAz4+mOg5XW7gVVZXWHWiRzTcpuq2PIOyo+qSgAt52NDwid8C+nQbN119eRZl5cglAUs7XPj1k0C3bt+mqzvPYjWb2sbgA4f+Wwi58zDw27p9W0IvK//Mnv17P6ZqtmAAIXduzxcwtFpD0TyTDUuTugBLt2vLyMoz2TDABrrG3EoSeSYbBlirTaUBa8BaVftR0fxRzdQYNlaGlJimgRUIYEgD3/dJuy5Zz8PzfQ24na01FHToikUJB4N561W5eEgpFpaWmZqZZWl5GV8pDbitxjkp6YpF6YrFMKTAVwq1AjH3MxR0CDk2k5dnmJqdW/X/egxuecuNRzrojseQebhrSSmFAro743TFohpwO0gphWNZxKMRhBBlQ+uMRgg5TkMga8A1uuZg0MG2rIpgGYZBLNKhAbdDxBysEG7BrdtWANMwNOCWdc95CzZNs6qjhcgdW28r3hSAA6aZc5NNwVz9pzbCRbd9miSlxHYcUArP88hksw2pIAnA9xVZz6/qaKUUWc+r+7W2tQXnxjILKQRSSgKWhZSNuyXP81laTiMrhKSUIu26eJ6nx+BSrjkQCKz7d73lK5+l9DLLrluRJWY9j5n5+YZ4mrYFLKXEcZz3/LsVCGA0IDoteJCl5TQzc1fKHk+VUszNL7C4tKwBF1NwDbiFHNOyrIbO5MzMXWF6dq5kuFWAe2lqumK3fl0FWZZlrWulSikCpolnmriZTINcteLS9GXSrkt3PEbgmtRJAZ7nMXl5htkr8w3tfG0H2DAMHNsu+T7btvF8vyGBTEGzV+aZm18gFAziWLmhwvd8llyXxaUllFINnyNuO8DrueY10yfbZinfsI0akwEWFheZX3x3SiWEaMoCgLYagx3HKTsNKrhqq4FR9WrQctWrmSs72gKwUgqzClhKKWzbblhU3ZLZRtukRGWMu+tavm23xPooDbgIoFoqVKZpYtfQQTTgernmfOGiuhmbd7tqKxB4T/qiATcZrinlhhYt7A101UopDbjWC7M2ePJgvfJmZXm4ZMe2rViBQFtAlq1qvQHLwqxDihMwTawqVmEUdMOWbiLhENu6uxo6c7VpACvANHILx+sV99pFSp3FXPLWrk46QiEAwkGHrV2dKF9pwBVdkBBYgUBdrUMIUXZFrAA3GumgKxZ91zmiHWE641F85WvAlbjQRszpSsPAtu2SrloBjm1xQ/d7t+OaUtIZjdIRCrfseNxSgA3DwLYas89cFFKwEq7akJId27et6VEUYFsBuuMxrIDVkpBbBrAQIhc1N7CsKITAcZyiY/2ObVuwiuTPSinCQYfuzlhLlkRbBnAgX4hotBUYhoG1RpVLKdjW1UmojLFaKUU8EqYzGmm5xyC0BOBGuub1OtfqalkOWAexSEdFhZGuWIxIR1gDvtZN2jXWmmvuYFJi5ytmSilCKy63/GtSKlcE6Y7HCbZQ3bvpgK0mueZrXaxhGAQCAWzLYmtXZ75SVfl5HNuiuzPeMnXvpl6FUWaq0ig5tk00HCRYwzX5vk8kHCKbzTIxNd3Qzd4tZcFCiJZyZQKIhIKEg8Gaz+X7PvFohM5Y858dJ5tpLUYLTd85tkUo6GxYFKyUojseI9rx7iKIQCKEgcy/hDAQ1O+rLprSwoVqVau4ZitgEgmHMKTc0GsypMENW7ahPJPltIuvPNLeLFmVxlNu7j3CwpQOtowgRQClPHw8lPJQG7CdruGAZT5qbhUZhkE0HMLcQLiGMDENi6XMHBeunOL0wi84P/M2ae8yWeXmIforFi2FgSlsbCNGJPA+4tZOOu1dBISDTxZfee0D2AkGW2aaTQhBJBTEMs0N2XpqyABSSM7NHueNiz/g3OzrLGeu4HpLuN4ySmVhrbxaKRAgyIE2pYMpg2yxb2VH+Dfptm/BV5mqQDcMcCuucAw5NkG79n3FUuTG1bMzxzjyzgtcnH8bN7tIVmVWAjgQCGGsH+HllVXLZLxl8GZYzE5yfvEI0cD7uSnax7bgh/F8tyLX3TDAZr5a1SqlPMcKEAuHaoZryAAzS+f5nzPf5O2pV/E8F4WfA1rV3V49yldZXJVlMv0WM5Nn6HZuoSf++4TNrXgq3VpRtOM4LbN01TQM4pGOGuHmql4nLozyraOf5+TED8l66bx1bfR9KrIqzcWl13hl4u84NfcSEevXfIC+ZH9zAa+45hYZd6UUxKMdNXY2QTq7wCtn/5n/OnmAxcxMldZaudLeHG/Ofuemfzv9yT/pTfaHRwZSFANd91Y3TTNXtmsR642Fw1g1xgFLmRkOv/0U/zv2LaQwGwZ3VQezFPyRgGRfsn87wMhAak3IdQW88oiFFrHejpBD0LFrcM2Cpcwsw6f+np//30tYZrDZt3Qv8A99yf4PFCD3Jvc2DrBtWTUvWt8oBW2LaDgMSuV2+1X6Ejm3/PLpJG9M/ADLCFX0+Z7ySr6qXNv1ceDRvmT/NoDRgUPc+fTe+kfRhmEgpcT3m78gTQpJ0LZIu25Ngc6R8f/g5+e/i2WGKj56V+eNRT2HQOB6LpMLk2T8ijeu/yEw3pvc+1ejA4eWDFPUH3DhOcnNlgJMIbjgpqt2zVKYnF98lR+f//eq3LKvfB7e8yW8IhZqSoOzM+/wjSNPMT43jhQVO9eHBOIXfQf7nx++L+X3JvsZHUjVD7DKP7eqFZSuoQQphGQxe4GfTfxrNY2+0hYfjH2grPdahlVtDdoCHkbwM+Ct0Xx0fV08yrCwu76alxQGb839J2n/SvX5bZmHZf2aJxg+BHy6L9nvFFIn/azKEq55cvkkE0uvNyEVqlqfAz7Ul+xnZCClARc3PMnpK6N4KttOl20Bn8n/1IDXt16D6fQvmXXfAVS7Xf4+hdq+J5nQgNcHbHF+8adk1XI7Xr4hEPcaLVMgbjnXLHC9eabSv2w397xaA9pFrxt1G8xkzpLxF9r5Nj4CbNVfq7OWe8ZgJj1G1l8uGTv7ys8t9SnyRs8vtx6g8P38F2jJ9Ss3K2lc6ci+VwNep7hxJXOBrHLXTWI95bGr80Ye3vOlsosYpXRjfCfJTzxd8n3n5sZ55PCjvDl1ioAsivB27aLXtEqPtHc5t4aqqL1RtPxYz+tTqHIy81vLBey1Y65Qbe6bW9rqtswcdg26uVzA04B7XZivEGRVGqW8zXA3nWUBVv6Zi8Dc9WHB4Cl3Zd1ymytSEvBjh0+xf+/HFPAakEGrrfprScB+eiUXfA6Y2uwtoshtJxGbo0QwV/Iu9vd/FIDBRM8PgZFN3+eVwpQOUhig2j6unC6rmw4Nnyj8+hd5V72JLVhhywimsEvO4woEpmz8Tg1T5lZyltH9TpdV6BhM9HBg+A0GE79xemj4xANAEvj1zYpYigC2EUNgFIXrei5nZ94BcpP1xc55Y3xnyU92PZdLi5dwvcy6BRYpJONz46S9dDl58MmyK1kyH1UOJnpeGRo+8XvAl4F7NqeX9ogE3ocp7PxsklizoScXJvnGkadKLrPxfa+sCtWlxUt8/dUnOTc7vu5SYwG4XobJxUmM0t7jRxVl8kPDxxlM7C647W3AHcBngcRmAiyFyVT6FMemnmXJu1y05usrv+QyG8/3eXlf6fBlbOYMf/n9Rxm7PFZ0LblAlLs+7IaKatGDid08NnychxK7GUz0TAwNn3gReBmIA7cAO4AobV71UsonZn3Q9ZT7RZG7pyKdQZbj/srOwqWUSCkxRM1j+zFgquLJhoeuWjCDiR6fXJVremj4+Jl82rUpvhzh84nbMn3Ju28F8QDQjt8H8CzgVz2bNJjo4UDqGFbn+3nw9i0MJnb7sDnKP6ts+V+AP2hTwM/jS7+mbH5//0d58PYtmzZlGhlI/Rj4aRt23CeBqZF9L+kVHevpzn9c2an3N0C7Le34ytGjLFc0/F9v+v6nUvxO8hOMDKRGaa8K3uPA2PQTKXoP6oXvRTXPfOHXzwETNdROykzPZK0R6hjw1ZGB1GLfc/2M3p9CL9kpZsUD3yO/Q+BXfcn+LwJfAyp+HL0QgnNz40WfkmNKk/G5cVwvU+0uigzwBbKcBWDxamFEq4TuOng337v/u/Ql+78OPFC5AStu6txVtCAiEKS9NBMLl8h4Vc3K/i3wyMhAaqHvYD8j9+c2n2kLLkNZuRJEDwJbyO2sL9+CEbw5daqoNRUe3WJIoxoLfh54fGQgtdD3TD8j96Uqr7Fc1676vkMFV70A/DHw7UrPEZAmZpFX4f+rgPsi8GcjA6kLdz1zN9eOAhpw+Tlx4efFfND1bEsUM+BPRwZSY5Bb4Tm6L6UBV6vCU2xGBlK/AvYDf01zFiNm8mPugwW4vf+0l9GBQ2sMD1oVQy5Yc29yb1Agfhd4mNzm60ZoDPgC8J2RgdTCXc/cnbPcNeBqwFWqN3nVWvoO9ksENwOfzrvuen67yOPAV8lyduRTKb/vmX6Ux3vcsga8Abrz6b0YpmD4vpUnzTl5K/4MsA/YyLU8TwJfAcZGBlKL+Y61kgoVj+C1arTm3NNsVo3RlkJtF4h7yW3h/EiVpz6WD+SeB6aOHmV5+okUfc/1wyJlwdWA6zQ270kmMFj5Tp6tQC9wO3ArcDPQCUTy7T9Hbk79NHAS+BFwmNwSZR9f+iP7Xsp1poO58mMl+n/6s2ZLWjToyQAAAABJRU5ErkJggg==";
    else
      return curImages[index];
  }


  $scope.getTNReadOnly = function(index) {
    //console.log("curimage in tn:"+curImages[index]);
    //console.log(curImages);
    if(curImages[index] == "null" || curImages[index] == "")
      return "null";
    else
      return curImages[index];
  }



  $scope.registerImage = function(img){
    var obj = {jobId: $scope.curJobId, imgIndex: $scope.curImgIndex, img: img};
  }



  $scope.doTakeCamera = function(){
    $scope.$apply();
    //console.log("imageindex:"+curImgIndex);
    $scope.getCameraImage();
    imageSheet.hide();
  }



  $scope.test = function(imgIndex){
    //$scope.curImgIndex = imgIndex;
    curImgIndex = imgIndex;

    //console.log("scope:"+curImgIndex+" local:"+imgIndex);
    imageSheet.show();
  }

  $scope.testGPS = function(){
    $scope.curGPS = androidGetGPS();
  }



  $scope.clickSuccess = function(){
    $scope.showMOD = true;
  }



  $scope.updateProg = function(){
    imageSheetProg.show();
  }



  $scope.backButton = function () {
    console.log("backbutton:"+$scope.pageStack);
    if($scope.pageStack != ""){
      if($scope.pageStack == "tab1detail"){
        myNavigator.popPage( { animation : 'none'} );
      }
      else if($scope.pageStack == "tab2detail"){
        myNavigator2.popPage( { animation : 'none'} );
      }
      else if($scope.pageStack == "tab3detail"){
        myNavigator3.popPage( { animation : 'none'} );
      }
    }
    else if($scope.pageStackRecon != ""){
      if($scope.pageStackRecon == "tab1detailRecon"){
        myNavigatorRecon.popPage( { animation : 'none'} );
      }
      else if($scope.pageStackRecon == "tab2detailRecon"){
        myNavigator2Recon.popPage( { animation : 'none'} );
      }
      else if($scope.pageStackRecon == "tab3detailRecon"){
        myNavigator3Recon.popPage( { animation : 'none'} );
      }
    }
  };




  $scope.doConfirmSuccess = function(){
    //console.log("stack:["+$scope.pageStack+"]["+$scope.pageStackRecon+"]");
    console.log("modrecon before:"+$scope.$root.showMODRecon);
    imageSheetProg.hide();
    if($scope.pageStack != "")
      $scope.$root.showMOD = true;
    else if($scope.pageStackRecon != "")
      $scope.$root.showMODRecon = true;
    console.log("modrecon:"+$scope.$root.showMODRecon);
    $scope.$apply();
  }


  $scope.doConfirmPending = function(){
    imageSheetProg.hide();
    if($scope.pageStack != "")
      $scope.showReason= true;
    else if($scope.pageStackRecon != "")
      $scope.showReasonRecon = true;
  //  $scope.$apply();
  }


  $scope.reasonTextChange = function(){
    if(document.getElementById("reason99").checked){
      $scope.reasonShow = true;
    }
    else{
      $scope.reasonShow = false;
    }
  }


  $scope.submitDisconSuccess = function(){
    //alert($scope.disconentry.remark);
    //return;
    $scope.$root.showLoader = true;
    if(TARGET == "device"){
      if(androidGetGPS())
        $scope.curGPS = androidGetGPS();
      else
        $scope.curGPS = "101.58239999999999,2.953228333333333";
    }
    else if(TARGET == "browser"){
      $scope.curGPS = "101.58239999999999,2.953228333333333";
    }
    console.log("gps before submit:"+$scope.curGPS);
    var submitData = [];
    var discon = {
      id: $scope.curJobId,
      img1:curImages[0],
      img2:curImages[1],
      meter_reading:$scope.disconentry.meter_reading,
      meterno_last4: $scope.disconentry.meterno_last4,
      discon_method: $scope.selectedMOD,
      gps: $scope.curGPS,
      remark: $scope.disconentry.remark
    };
    submitData.push(discon);
    $http({
        method: 'POST',
        url: 'http://'+getAPIHost()+'/api/updatedisconsuccess',
        data: submitData,
        headers: {'Content-Type': 'application/json'}
    })
    .then(function successCallback(data) {
        console.log(data.data);
        $scope.$root.showLoader = false;
        //$scope.infoDlgShow = true;
    }, function errorCallback(data) {
        console.log("cannot connect");
        console.log(data);
        $scope.$root.showLoader = false;
    });
  }


  $scope.submitDisconPending = function(){
    $scope.$root.showLoader = true;
    var submitData = [];
    var discon = {
      id: $scope.curJobId,
      img1:curImages[0],
      img2:curImages[1],
      meter_reading:$scope.disconentry.meter_reading,
      meterno_last4: $scope.disconentry.meterno_last4,
      discon_reason: $scope.selectedReason,
      reason_text: $scope.reason.text,
      remark: $scope.disconentry.remark
    };
    submitData.push(discon);
    $http({
        method: 'POST',
        url: 'http://'+getAPIHost()+'/api/updatedisconpending',
        data: submitData,
        headers: {'Content-Type': 'application/json'}
    })
    .then(function successCallback(data) {
        console.log(data.data);
        $scope.reason.text = "";
        //$scope.infoDlgShow = true;
        $scope.$root.showLoader = false;
    }, function errorCallback(data) {
        console.log("cannot connect");
        console.log(data);
        $scope.$root.showLoader = false;
    });
  }




  $scope.loadJobImages = function(jobId){
      $http({
          method: 'GET',
          url: 'http://'+getAPIHost()+'/api/getjobimages/'+jobId,
          headers: {'Content-Type': 'application/x-www-form-urlencoded'}
      })
      .then(function successCallback(data) {
         console.log(data.data);
         curImages[0] = data.data[0].img1;
         curImages[1] = data.data[0].img2;
      }, function errorCallback(data) {
          //console.log("cannot connect");
      });
  }


  $scope.loadAssignedJobList = function(){
      $scope.$root.showLoader = true;
      $http({
          method: 'GET',
          url: 'http://'+getAPIHost()+'/api/getassignedjoblist',
          headers: {'Content-Type': 'application/x-www-form-urlencoded'}
      })
      .then(function successCallback(data) {
        //  console.log(data);
          $scope.assJobs = data.data;
          $scope.assCount = $scope.assJobs.length;
          console.log($scope.assJobs);
          $scope.$root.showLoader = false;
      }, function errorCallback(data) {
          //console.log("cannot connect");
          $scope.$root.showLoader = false;
      });
  }


  $scope.loadPendingJobList = function(){
    $scope.$root.showLoader = true;
      $http({
          method: 'GET',
          url: 'http://'+getAPIHost()+'/api/getpendingjoblist',
          headers: {'Content-Type': 'application/x-www-form-urlencoded'}
      })
      .then(function successCallback(data) {
        //  console.log(data);
          $scope.pendJobs = data.data;
          $scope.pendCount = $scope.pendJobs.length;
          $scope.$root.showLoader = false;
          console.log($scope.compJobs);
      }, function errorCallback(data) {
        $scope.$root.showLoader = false;
          //console.log("cannot connect");
      });
  }


  $scope.loadCompleteJobList = function(){
      $scope.$root.showLoader = true;
      $http({
          method: 'GET',
          url: 'http://'+getAPIHost()+'/api/getcompletejoblist',
          headers: {'Content-Type': 'application/x-www-form-urlencoded'}
      })
      .then(function successCallback(data) {
        //  console.log(data);
          $scope.compJobs = data.data;
          $scope.compCount = $scope.compJobs.length;
          console.log($scope.compJobs);
          $scope.$root.showLoader = false;
      }, function errorCallback(data) {
          //console.log("cannot connect");
          $scope.$root.showLoader = false;
      });
  }


  $scope.getListCount = function(){
      $http({
          method: 'GET',
          url: 'http://'+getAPIHost()+'/api/getlistcount',
          headers: {'Content-Type': 'application/x-www-form-urlencoded'}
      })
      .then(function successCallback(data) {
          console.log(data.data);
          $scope.listCount = data.data;
      }, function errorCallback(data) {
          //console.log("cannot connect");
      });
  }
  //$scope.loadAssignedJobList();
  if(TARGET == "device")
    androidGetGPS();


















// ************** RECON SEGMENT ************************************************
$scope.assJobsRecon = {};
$scope.pendJobsRecon = {};
$scope.compJobsRecon = {};



$scope.getAllCountRecon = function(){
  var assCounter = 0;
  var pendCounter = 0;
  var compCounter = 0;

  if($scope.getAssCountRecon() === undefined)
    assCounter = 0;
  else
    assCounter = $scope.getAssCountRecon();

  if($scope.getPendingCountRecon() === undefined)
    pendCounter = 0;
  else
    pendCounter = $scope.getPendingCountRecon();

  if($scope.getCompleteCountRecon() === undefined)
    compCounter = 0;
  else
    compCounter = $scope.getCompleteCountRecon();

  if(assCounter + pendCounter + compCounter == 0)
    return;
  else
    return assCounter + pendCounter + compCounter;
}

$scope.getAssCountRecon = function(){
  if($scope.listCount === undefined) return;
  if($scope.listCount[3] > 0)
    return $scope.listCount[3];
}

$scope.getPendingCountRecon = function(){
  //console.log("count 2:"+$scope.listCount[1]);
  if($scope.listCount === undefined) return;
  if($scope.listCount[4] > 0)
    return $scope.listCount[4];
}

$scope.getCompleteCountRecon = function(){
  if($scope.listCount === undefined) return;
  if($scope.listCount[5] > 0)
    return $scope.listCount[5];
}



$scope.onshowrecon = function(whichPage){
  console.log("onshowrecon:"+whichPage);
  if(whichPage == "tab1listRecon"){
    $scope.$root.successWording = "Success reconnection";
    $scope.$root.pendingWording = "Pending reconnection";
    $scope.pageStack = "";
    $scope.pageStackRecon = "tab1listRecon";
    $scope.loadAssignedJobListRecon();
    $scope.getListCount();
  }
  else if(whichPage == "tab1detailRecon"){
    $scope.pageStackRecon = "tab1detailRecon";
  }
  else if(whichPage == "tab2listRecon"){
    $scope.pageStackRecon = "tab2listRecon";
    $scope.loadPendingJobListRecon();
    $scope.getListCount();
  }
  else if(whichPage == "tab2detailRecon"){
    $scope.pageStackRecon = "tab2detailRecon";
    $scope.loadJobImagesRecon($scope.curJobId);
  }
  else if(whichPage == "tab3listRecon"){
    $scope.pageStackRecon = "tab3listRecon";
    $scope.loadCompleteJobListRecon();
    $scope.getListCount();
  }
  else if(whichPage == "tab3detailRecon"){
    $scope.pageStackRecon = "tab3detailRecon";
    $scope.loadJobImagesRecon($scope.curJobId);
  }
}



$scope.doPushPageRecon = function(page, jobId){
  if(page == "tab1detailRecon"){
    $scope.showMODRecon = false;
    $scope.disconentry = {};
    curImages = ["null", "null"];
    $scope.curJobId = jobId;
    myNavigatorRecon.pushPage('tab1detailRecon.html', {data: {title: 'Page 2'}});
  }
  else if(page == "tab2detailRecon"){
    curImages = ["null", "null"];
    $scope.curJobId = jobId;
    myNavigator2Recon.pushPage('tab2detailRecon.html', {data: {title: 'Page 2'}});
  }
  else if(page == "tab3detailRecon"){
    curImages = ["null", "null"];
    $scope.curJobId = jobId;
    myNavigator3Recon.pushPage('tab3detailRecon.html', {data: {title: 'Page 2'}});
  }
}



$scope.onhiderecon = function(whichPage){
  console.log("onhiderecon:"+whichPage+"<>"+$scope.pageStackRecon);
  if(whichPage == "tab1detailRecon" && $scope.pageStackRecon == whichPage){
    myNavigatorRecon.popPage( { animation : 'none'} );
  }
  else if(whichPage == "tab2detailRecon" && $scope.pageStackRecon == whichPage){
    myNavigator2Recon.popPage( { animation : 'none'} );
  }
  else if(whichPage == "tab3detailRecon" && $scope.pageStackRecon == whichPage){
    myNavigator3Recon.popPage( { animation : 'none'} );
  }
}


$scope.clickOKMODRecon = function(){
  $scope.showMODRecon = false;
  if(document.getElementById("r1recon").checked)
    $scope.selectedMOD = "fuse";
  else if(document.getElementById("r2recon").checked)
    $scope.selectedMOD = "meter";
  else if(document.getElementById("r3recon").checked)
    $scope.selectedMOD = "wire";
  console.log($scope.selectedMOD);
  ons.notification.confirm({message: 'Please confirm to update success reconnection & print reconnection success letter?'})
  .then(function(answer) {
    if(answer==1){
      $scope.submitDisconSuccessRecon();
      var img1 = "";
      var img2 = "";
      var imgCount = 0;
      var printImg = false;
      if(curImages[0] != "null" && curImages[0] != ""){
        img1 = curImages[0];
        imgCount++;
      }
      if(curImages[1] != "null" && curImages[1] != ""){
        img2 = curImages[1];
        imgCount++;
      }
      if(imgCount > 0) printImg = true;
      if(TARGET == "device")
        printReconSuccess($scope.getCurDataRecon().customer, $scope.getCurDataRecon().addrs1, $scope.getCurDataRecon().addrs2, $scope.getCurDataRecon().addrs3, $scope.getCurDataRecon().acctno, $scope.getCurDataRecon().sono, $scope.getCurDataRecon().ar_ob, $scope.getCurDataRecon().ar_ad, $scope.getCurDataRecon().ar_rd, img1, img2, printImg, imgCount, $scope.selectedMOD);
      $scope.infoDlgTitle = "Submitted";
      $scope.infoDlgBody = "Progress submitted & reconnection letter sent for printing";
      $scope.infoDlgShowRecon = true;
    }
  });
}



$scope.backTab1DetailRecon = function () {
  console.log($scope.pageStackRecon);
  if($scope.pageStackRecon == "tab1detailRecon"){
    $scope.infoDlgShowRecon = false;
    myNavigatorRecon.popPage( { animation : 'none'} );
  }
};



$scope.getCurDataRecon = function(){
  //console.log("pgstack:"+$scope.pageStackRecon);
  if($scope.pageStackRecon == "tab1detailRecon"){
    for(i=0; i<$scope.assJobsRecon.length; i++){
        if($scope.assJobsRecon[i].id == $scope.curJobId){
          //console.log($scope.assJobs[i].id);
          return $scope.assJobsRecon[i];
        }
    }
  }
  else if($scope.pageStackRecon == "tab2detailRecon"){
    for(i=0; i<$scope.pendJobsRecon.length; i++){
        if($scope.pendJobsRecon[i].id == $scope.curJobId){
          //console.log($scope.pendJobsRecon[i]);
          return $scope.pendJobsRecon[i];
        }
    }
  }
  else if($scope.pageStackRecon == "tab3detailRecon"){
    for(i=0; i<$scope.compJobsRecon.length; i++){
        if($scope.compJobsRecon[i].id == $scope.curJobId){
          //console.log($scope.compJobs[i]);
          return $scope.compJobsRecon[i];
        }
    }
  }
}



$scope.clickCancelMODRecon = function(){
  $scope.showMODRecon = false;
}



$scope.clickOKReasonRecon = function(){
  $scope.showReasonRecon = false;
  if(document.getElementById("reason1recon").checked)
    $scope.selectedReason = "reason1";
  else if(document.getElementById("reason2recon").checked)
    $scope.selectedReason = "reason2";
  else if(document.getElementById("reason3recon").checked)
    $scope.selectedReason = "reason3";
  else if(document.getElementById("reason99recon").checked){
    $scope.selectedReason = "reason99"
    //$scope.reasonTxt = document.getElementById("reasontext").txt();
  }
  //console.log($scope.reasonTxt);
  ons.notification.confirm({message: 'Please confirm to update pending reconnection & print reconnection pending letter?'})
  .then(function(answer) {
    if(answer==1){
      $scope.submitDisconPendingRecon();
      var img1 = "";
      var img2 = "";
      var imgCount = 0;
      var printImg = false;
      if(curImages[0] != "null" && curImages[0] != ""){
        img1 = curImages[0];
        imgCount++;
      }
      if(curImages[1] != "null" && curImages[1] != ""){
        img2 = curImages[1];
        imgCount++;
      }
      if(imgCount > 0) printImg = true;
      if(TARGET == "device")
        printReconFail($scope.getCurDataRecon().customer, $scope.getCurDataRecon().addrs1, $scope.getCurDataRecon().addrs2, $scope.getCurDataRecon().addrs3, $scope.getCurDataRecon().acctno, $scope.getCurDataRecon().sono, $scope.getCurDataRecon().ar_ob, $scope.getCurDataRecon().ar_ad, $scope.getCurDataRecon().ar_rd, img1, img2, printImg, imgCount);
      $scope.infoDlgTitle = "Submitted";
      $scope.infoDlgBody = "Progress submitted & reconnection pending notice sent for printing";
      $scope.infoDlgShowRecon = true;
    }
  });
}



$scope.clickCancelReasonRecon = function(){
  $scope.showReasonRecon = false;
}


$scope.clickSuccessRecon = function(){
  $scope.showMODRecon = true;
}


$scope.getReasonRecon = function(id){
  if(id == "")
    id = $scope.curJobId;

  for(i=0; i<$scope.pendJobsRecon.length; i++){
    if($scope.pendJobsRecon[i].id == id){
      if($scope.pendJobsRecon[i].fail_reason == "reason99")
        return "Other reason: " + $scope.pendJobsRecon[i].reason_text;

      if($scope.pendJobsRecon[i].fail_reason == "reason1")
        return "Reason 1";

      if($scope.pendJobsRecon[i].fail_reason == "reason2")
        return "Reason 2";

      if($scope.pendJobsRecon[i].fail_reason == "reason3")
        return "Reason 3";
    }
  }
}



$scope.getMOR = function(id){
  if(id == "")
    id = $scope.curJobId;

  for(i=0; i<$scope.compJobsRecon.length; i++){
    if($scope.compJobsRecon[i].id == id){
      if($scope.compJobsRecon[i].discon_method == "fuse")
        return "DOF - Fuse";

      if($scope.compJobsRecon[i].discon_method == "wire")
        return "DOW - wire";

      if($scope.compJobsRecon[i].discon_method == "meter")
        return "DOM - meter";
    }
  }
}



$scope.loadJobImagesRecon = function(jobId){
    $http({
        method: 'GET',
        url: 'http://'+getAPIHost()+'/api/getjobimagesrecon/'+jobId,
        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
    })
    .then(function successCallback(data) {
       console.log(data.data);
       curImages[0] = data.data[0].img1;
       curImages[1] = data.data[0].img2;
    }, function errorCallback(data) {
        //console.log("cannot connect");
    });
}



$scope.loadAssignedJobListRecon = function(){
    $scope.$root.showLoader = true;
    $http({
        method: 'GET',
        url: 'http://'+getAPIHost()+'/api/getassignedjoblistrecon',
        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
    })
    .then(function successCallback(data) {
      //  console.log(data);
        $scope.assJobsRecon = data.data;
        $scope.assCountRecon = $scope.assJobsRecon.length;
        console.log($scope.assJobsRecon);
        $scope.$root.showLoader = false;
    }, function errorCallback(data) {
        //console.log("cannot connect");
        $scope.$root.showLoader = false;
    });
}



$scope.loadPendingJobListRecon = function(){
  $scope.$root.showLoader = true;
    $http({
        method: 'GET',
        url: 'http://'+getAPIHost()+'/api/getpendingjoblistrecon',
        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
    })
    .then(function successCallback(data) {
      //  console.log(data);
        $scope.pendJobsRecon = data.data;
        $scope.pendCountRecon = $scope.pendJobsRecon.length;
        $scope.$root.showLoader = false;
        //console.log($scope.compJobs);
    }, function errorCallback(data) {
      $scope.$root.showLoader = false;
        //console.log("cannot connect");
    });
}



$scope.loadCompleteJobListRecon = function(){
    $scope.$root.showLoader = true;
    $http({
        method: 'GET',
        url: 'http://'+getAPIHost()+'/api/getcompletejoblistrecon',
        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
    })
    .then(function successCallback(data) {
      //  console.log(data);
        $scope.compJobsRecon = data.data;
        $scope.compCountRecon = $scope.compJobsRecon.length;
        //console.log($scope.compJobs);
        $scope.$root.showLoader = false;
    }, function errorCallback(data) {
        //console.log("cannot connect");
        $scope.$root.showLoader = false;
    });
}


$scope.submitDisconSuccessRecon = function(){
  //alert($scope.disconentry.meter_reading);
  //return;
  $scope.$root.showLoader = true;
  if(TARGET == "device")
    $scope.curGPS = androidGetGPS();
  else if(TARGET == "browser")
    $scope.curGPS = "101.58239999999999,2.953228333333333";
  console.log("gps before submit:"+$scope.curGPS);
  var submitData = [];
  var discon = {
    id: $scope.curJobId,
    img1:curImages[0],
    img2:curImages[1],
    meter_reading:$scope.disconentry.meter_reading,
    meterno_last4: $scope.disconentry.meterno_last4,
    discon_method: $scope.selectedMOD,
    gps: $scope.curGPS,
    remark: $scope.disconentry.remark
  };
  submitData.push(discon);
  $http({
      method: 'POST',
      url: 'http://'+getAPIHost()+'/api/updatedisconsuccessrecon',
      data: submitData,
      headers: {'Content-Type': 'application/json'}
  })
  .then(function successCallback(data) {
      console.log(data.data);
      $scope.$root.showLoader = false;
      //$scope.infoDlgShow = true;
  }, function errorCallback(data) {
      console.log("cannot connect");
      console.log(data);
      $scope.$root.showLoader = false;
  });
}



$scope.submitDisconPendingRecon = function(){
  $scope.$root.showLoader = true;
  var submitData = [];
  var discon = {
    id: $scope.curJobId,
    img1:curImages[0],
    img2:curImages[1],
    meter_reading:$scope.disconentry.meter_reading,
    meterno_last4: $scope.disconentry.meterno_last4,
    discon_reason: $scope.selectedReason,
    reason_text: $scope.reasonRecon.text,
    remark: $scope.disconentry.remark
  };
  submitData.push(discon);
  $http({
      method: 'POST',
      url: 'http://'+getAPIHost()+'/api/updatedisconpendingrecon',
      data: submitData,
      headers: {'Content-Type': 'application/json'}
  })
  .then(function successCallback(data) {
      console.log(data.data);
      $scope.reasonRecon.text = "";
      //$scope.infoDlgShow = true;
      $scope.$root.showLoader = false;
  }, function errorCallback(data) {
      console.log("cannot connect");
      console.log(data);
      $scope.$root.showLoader = false;
  });
}


/*
$scope.getListCountRecon = function(){
    $http({
        method: 'GET',
        url: 'http://'+getAPIHost()+'/api/getlistcountrecon',
        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
    })
    .then(function successCallback(data) {
        console.log(data.data);
        $scope.listCountRecon = data.data;
    }, function errorCallback(data) {
        //console.log("cannot connect");
    });
}
*/





});
