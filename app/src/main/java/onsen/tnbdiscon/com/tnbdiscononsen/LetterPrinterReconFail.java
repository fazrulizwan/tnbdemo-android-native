package onsen.tnbdiscon.com.tnbdiscononsen;

import android.graphics.Bitmap;
import android.os.Looper;
import android.util.Log;

import com.zebra.sdk.comm.BluetoothConnection;
import com.zebra.sdk.comm.Connection;
import com.zebra.sdk.comm.ConnectionException;
import com.zebra.sdk.graphics.internal.ZebraImageAndroid;
import com.zebra.sdk.printer.PrinterLanguage;
import com.zebra.sdk.printer.ZebraPrinter;
import com.zebra.sdk.printer.ZebraPrinterFactory;
import com.zebra.sdk.printer.ZebraPrinterLanguageUnknownException;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by izwan on 20-Aug-18.
 */

public class LetterPrinterReconFail {
    private ZebraPrinter printer;
    private Connection printerConnection;

    String custName, addrs1, addrs2, addrs3, acctNo, orderNo, ob, ad, rd;
    //WorkOrderDetail ctx;
    String img1, img2, img3;
    boolean printImg;
    int imgSize;

    public LetterPrinterReconFail(String custName, String addrs1, String addrs2, String addrs3, String acctNo, String orderNo, String ob, String ad, String rd, String img1, String img2, String img3, boolean printImg, int imgSize){
        this.custName = custName;
        this.addrs1 = addrs1;
        this.addrs2 = addrs2;
        this.addrs3 = addrs3;
        this.acctNo = acctNo;
        this.orderNo = orderNo;
        this.ob = ob;
        this.ad = ad;
        this.rd = rd;
        this.img1= img1;
        this.img2 = img2;
        this.img3 = img3;
        this.printImg = printImg;
        this.imgSize = imgSize;
    }



    public int print(){
        //ctx.showToast("Printing...");
        new Thread(new Runnable() {
            public void run() {
                Looper.prepare();
                doConnectionTest();
                Looper.loop();
                Looper.myLooper().quit();
            }
        }).start();
        return 0;
    }

    private void doConnectionTest() {

        if(printer != null) disconnect();
        printer = connect();
        if (printer != null) {
            Bitmap image;
            if(printImg) {
                if (!img1.equals("")) {
                    image = AppUtil.convertToMono(AppUtil.base64ToBitmap(img1));
                    try {
                        sendTestLabel2();
                        printer.printImage(new ZebraImageAndroid(image), 200, 10, 400, 400, false);
                    } catch (ConnectionException e) {
                        e.printStackTrace();
                    }
                }

                if (!img2.equals("")) {
                    image = AppUtil.convertToMono(AppUtil.base64ToBitmap(img2));
                    try {
                        sendTestLabel2();
                        printer.printImage(new ZebraImageAndroid(image), 200, 10, 400, 400, false);
                    } catch (ConnectionException e) {
                        e.printStackTrace();
                    }
                }

                if (!img3.equals("") && imgSize!=3) {
                    image = AppUtil.convertToMono(AppUtil.base64ToBitmap(img3));
                    try {
                        sendTestLabel2();
                        printer.printImage(new ZebraImageAndroid(image), 200, 10, 400, 400, false);
                    } catch (ConnectionException e) {
                        e.printStackTrace();
                    }
                }
            }

            sendTestLabel();

        } else {
            disconnect();
        }

    }

    private void sendTestLabel2() {
        try {
            byte[] configLabel = getConfigLabel2();
            printerConnection.write(configLabel);

            //setStatus("Sending Data", Color.BLUE);
            sleeper(1500);
            if (printerConnection instanceof BluetoothConnection) {
                String friendlyName = ((BluetoothConnection) printerConnection).getFriendlyName();
                //setStatus(friendlyName, Color.MAGENTA);
                //sleeper(500);
                //ctx.showToast("Print Success");
            }
        } catch (ConnectionException e) {
            //setStatus(e.getMessage(), Color.RED);
            //ctx.showToast("ERROR: unable to send data");
        } finally {
            //disconnect();
        }
    }

    private void sendTestLabel() {
        try {
            byte[] configLabel = getConfigLabel();
            printerConnection.write(configLabel);

            //setStatus("Sending Data", Color.BLUE);
            sleeper(1500);
            if (printerConnection instanceof BluetoothConnection) {
                String friendlyName = ((BluetoothConnection) printerConnection).getFriendlyName();
                //setStatus(friendlyName, Color.MAGENTA);
                //sleeper(500);
                //ctx.showToast("Print Success");
            }
        } catch (ConnectionException e) {
            //setStatus(e.getMessage(), Color.RED);
            //ctx.showToast("ERROR: unable to send data");
        } finally {
            disconnect();
        }
    }


    private byte[] getConfigLabel() {
        PrinterLanguage printerLanguage = printer.getPrinterControlLanguage();

        SimpleDateFormat fmt = new SimpleDateFormat("dd MMM yyyy hh:mm");
        Date dt = new Date();
        String dtStr = fmt.format(dt);

        double dblOB = Double.parseDouble(ob);
        double dblAD = Double.parseDouble(ad);
        double dblRD = Double.parseDouble(rd);
        double total = dblOB + dblAD + dblRD;
        String strTotal = String.valueOf(total);

        byte[] configLabel = null;
        String zplStr = "";
        if (printerLanguage == PrinterLanguage.ZPL) {
            //configLabel = "^XA^FO17,16^GB379,371,8^FS^FT65,255^A0N,135,134^FDTEST^FS^XZ".getBytes();
            /*
            zplStr =
                    //"~DYE:LOGO2.PNG,P,P,3667,,iVBORw0KGg0KICAgDQpJSERSICABPyAgAcEBAyAgICXwLoEgICABc1JHQiCuzhzpICAgBGdBTUEgILGPC/xhBSAgIAZQTFRFICAg////pdmf3SAgIAlwSFlzICAuIiAgLiIBquLdkiAgDQrWSURBVHja7dy7buU2GiBgcjgwUwzMLbcIzHkEl1MYwzzKPIJLF4alIA8wj7CvwiBFyrwCgy22VbCNFhDE/S/U0Y0XBVlsNScziX38xUfi5edPipKI+PKi8tIoZoKTqL5WONShvcG+DvUC57oTcoFjA4ouwZDOLP/ydJAEfc3hGegEhXA1OA0KxXCmo628hGA4DQpThx7OBuFQ/2QGCIOoO6hhQ9A3DhEOUhPsdQv2imDrXODgJMK5dS54NggnrMr6axQdwLF10ljaDuAgGw0KDQokwaAaTRzaTm8Bet1o4vBzbxCaRhOHQwsaYG9bTbyLgwIIxdjshSPBrt7/BdTcKGdBtV2HKk4gJ6zt+gtrGqBsRRQscvEmRnWlXzMcWtDF/rMYdBtaggYjRf1lon8QwbSKEWs7ILStYiR4j7DloMSHe+FdG0qG8xV4J/oLEDoWwG76C1AfulE3vQOYqWpzaHoIxblNPEOd7Q/cETxWtfan3uHmHHS9OI6OWDLyBCN20P1bNgf1JDAciscTPLQyM0LpTAcYM9AOgtry8wGqI3RBUFveQpODXY99BKGvQxrx4L+DCAd46Akz1tgFKAFR9Ycd7E9QjVxZMsjQgvICfBR64KJVXvoNCoQyDCA9QlWB8GnG81u61/1dEUJTJNgTFN8VISiboIF//l6Etsc/GivGiAfxsQZdgg/iVfxQhI46gsae9T0E7f4NCnySoyoXT0d/CH7Sw4cyfFvhix1eynBe4dD9MVyDlAWW4HSDmAv1RShXqOI2CBzglw0K1LtEYw+llyPDSHATN/dQ9wmqSBFyE2oq0G5P+iANCnUs11Mzc7vIt4d2gZLhJjjv4cMNCj3loKIOPfVCt8sL9vAzQegNCoKy2u0YsIdPDAMNChT7cL+HrzcIQcrtuvsevhA08Ju6mUNkHY6UbEBhFeDfxEitB6E9xswtfIbIiRBGD4BDEcphgbMQp8RlA1UgKmduEbry0QahIIihAv4WoCVIA5Sn2G4NCrDzGInTSIYDlS2UYyQIgxFV9MxfZiB8oELILQLHmUIV6hV2AaEsQPgNCqFjTBO3bVGu0CY40GnRfC0PHUOL3bDHHNUW2mOCZmIYig0KFyH8BOYOAqdicD6FrmAZYjCJdHyxDLENCuEbaBiWZoD5XghfYTyBrk8QDrMO4XMVdqxelyA0LGy1DJ+6KsS4DLFMj+K1681k8yEFmyv+5I0hjJ6yDHEEnLFfv3TYWWHMyUCMdBBC4eMBjh3EIfyThR6bqiQ4dBCHdhFg18Ip4E3UeTrxad9pDjBgTGEIGdy2G247V8B88Aa/FCEmgyOWjYNe1mG2tY7Wh36NUQeyIwedpw0KB4SGocPUsSDhc9UNCjHbkuJ9DmqKjpBvOa99DQrCrxN4ylDsN5jNKbANCrwc4H0OYpNYf+OP9F4edgQ9QcvwIQsd9i6H0Hj7E6052CyEpnqELgt1uMHe/lKBalB7OFFLPkPI2QIO1ghNDQqKZ32EMZ8WPmosRIb/pCWZAvx4hNjhstDcoP53DQr+wNAtcKDkOQP9EWKYyUKboMWghysy+Y+W/Q7+Iwb8aRt+jcH+loVqgd72EJhN9PZrFmquaGiSBGHMc6YMHULDsH+zWXh/grPt31egCTBXibOKP072NvPZwgccVrdQjrY3Gfh0hGqwog0KB4DdJGcdIPZn+IoQp0cLhD6GU6sTfDlAOBWXhSNGZ5yZjfBTB9AhDG0IY4jLQXmDE3ayUYwz9EI7nOGA0OAIwhB64VsOKoYwcYVyQSC9cM5BTRCMWOCAcDzDQBAzPvgXNM0YIFS7KlQMfR5CoglfYKbS/TTgsUJeMVXguEL85ukMrV+g+2VkiLlACWICYn8fsTznAoSxEvKnDQqkYeylBDEBMX+MOMXnpGGowf9MCEcs9QzsGOKskOFAQ3wG9gtUMUFXgzTZmxFCfAkqA2OCnlZtGcLIPagCpKkZQhhfo9cg+wwc0owr8ooLJIdh0H4PZYI443IM4S3jB+41NQjRVFSgHjkxZmj7wJ1hCwXm8zTjMgxhTgWQW+QZKprVA1Q4+XKQVB2gQjjR/4DvIIQsv5OBU90dFAuUNzjL33n2fIAzlSemiAjtqGb5rzKkqY/y76CV6Umm1YoDxPc6mv4ofwfQjOoE9QJx6mMHhA6KRqWFkiOE9xzNnAn6bqhAi9NXhj/jSByXqcQOep76qFH5e9H9WoY4mcIDZUhDNi+AZCCN5q8JepOFXRzSuuVTgn0Jjmm19oPyD3TpyEZepDnA38a0UHyHQyMlhnn4K82RQL9jONGlsiOE1OhnrOy04H5PE74C9FSHy8wISRd5friHLuDk+7bksVxIzEGIE+tiBtZJCVqqmrBAviiag5qmrykwdbNQJYixXi8zR2hHGYgpoA0KOGFQy2IU/Hqdgx4jLK/+pfLBBfQCpDnkZsHMFiEtaKzLBDaeaiZBWn5Y5+ouB2k5GmNXt06YuzPEZgix0dLy0e0yBV35DDloeEExnY0sQ1w60rezofI+9EJMIaCE8VDVDQryNeM+D0e61rn9jRk4UjBdIR+j2EMIwbT+H7dVw5d7C9BvLoZ2MR5Dc4JidmGzUOgy0E2UJ8ww2V0v99nUJjLwDQrGj3WOoFMNCu6ylCmtNOM6ygJVqpgdnBn2MEqvl6SWJew9xMwD5h6DXOHSQLYpV2TocRy6QZ3aZgYGFberKM4fIYUzWjSM4VUcXzvYM4S491SDKkGIYPOnOkwzhcOK2QnKBMfslfQdDAnaOhS44umx8eYuKu9WFYYE9TU4nRdxD9DRcoaaxGYS/NegpbUZNQmZufq8W9Cg7BfapJwbcE5QNKCOaXFITFcg/rqxDuFkNX/uSx1Kmpdg635uQY85IcAvdYhzp2uwC1jUTqyX6g0K0A0KCYoWpOUY24b2KsSc1gvTt+GMWeYFqAlq34QQdCAdvSChDuHEVbgC4cRlG0JmjfF5aMMuYN56AbrB5JvtEUKGHrPN9gRHjKZPbWgm/PpzG+oJx3XbhjArgdBo2lBGhHdtCM0Rwve7CxCS3MIelTOcL0HIhuMlaIercHSxvwLN5KK/AvVkCxuXDlDNtrAV6iBlNNegiF8L27WOsPvtKozVj15P1MXqWa8/s7Fajtt9AdWaWaG+DQpVaXucPUBZao8MNyVSauEn2I1XYWF7nCO4+S0uXITWX4SmsD2uI7g5Uy0uQlWAkeCVrW4XoWQYL8PmTkB1FeoE/VV4YVsjwwsbJRk292i6q7BLsFniMcH2htMFNrewLrBRkOYGL2x2Z9jcuLvA5lbgBdYPUm9g9bPdBtbahYlb2Hx9g9/g/wtyt+aYH7k3US/C3jelZbY8TCuGiqEpw2WUbUM3Ld1qrEPL31iCugLNtHTABtQ3OKTlswJU/I3+EzDUodxC2YbqT0DP5Z6FkTbbCLqusof0XzHxl7RwSJCWIvsDpE9bIC8ugE9wDQrN2KrUCdK9PQc4pd3eDKcNCkxtYxOazQnSG0L0B2hL0G/hsIObY5wRrgMSXdc7FQ9DuYOericskJZ9aB0Wj8JvxkKE3E0krwUqLnCCYQt7IVeYGi68afCEdlAsuZw8dAWawuu4GTT1Dt4614jXlVdInXcLb911WC4G3MZru4MpIHTLUvUmA3A72KVvcNI7bmA4QrtA2o+1Zin+tqS/P+suA+OpHOlkPC0ZxzVBUnuoluIheMuk5nXTsdz1wuX+liU34xi4bRRigf0OUjHGbXssQCrGuG3h4pX6tUnpnVtT1x2cGfLG0y2kYjxDaDYpxbIrjJdgT0V6AS4jyuFkaJdcOvtlUnEZ6h0cbjAld3o3TUkNCoav+l6C+GKo2jDFx4FLT/2PYVhmSBm4BKnAQ00TyiZUCfo0HBahvkFTh/YguxJ0CfZpbC/CyBBGBFuFFNrw7x5yV1Pkad8zRlhqpyrwPbHyW6L5DQoWIDSb97hAaoLzn+SEd6wGGBrxiqkd7vFio912hQ4aFUPcpmRpGzt07HvcMr0LKRBtEsQr2Wkokgg/7SPFixBfIIoJb5aiOyjxpgmEj/t+/XyDPUG6vi+es/AjwleDd2vhtVC8IMBw7dffwa/T/QKfRIDjG1Qw/SPAL0YsY6EadQzGE3y6h8QHoJykt/7xXkJZ9TtowwIjQgXQrVDcoHeBTuZ7+JH4Qpt7+i4Q1AcYwwJ7gDAY9ZClIORdPpKgwFtFAxbuhw+KYWTI906e4Xdb+CrwrF94dDvC9xv4QvCVL70f4IczfONx9AjFCXYe939QxgNQrVAeIN/5neCteO7FphwRQvF8BvhuhVwzO8g14126O3VXhUZs6nqB/Q5yo0D4RnDcNIoVYjOj9mh6A9UTxJ14Vl771My68Q6LhWBquAhxaLvjhovwozhCbD3GU1fA1VLsDQoJ4l2+3jJ84V4IkDZ/au5cj6krDHQnMkGoUYKBuiteasNx7pG6q6Rblg0KwxQgINLVZNzaHbm7PvFN0IOJM9274u3ohskNCtBTKeWh+9wnxyHFP4hBX4lRl2H/WYzqKpRXIFTrdAXiDQrvs7gMuzbEe+3jFYh378fetSE9OKC3bUiPIvCmDQo9wnChanqTnqvQeuG2FXpSQ+PFj3S48OyHiR4ScelpEvx8imb5DPLiEy88Pxqjb552r689lQM3EfFzPhpnM9Gd8nhOjbNZnhwyiUYl9ulZJLMQjUOU6Xkpff1JHwPPRmOaNdQ+2SQ4iNrpDOszXQ0Kl8vW1/KUmCvXFRg2LhfcnmTTulzgbvDCVYB53cRXfGGJxPm/UP1pBH/xwWggICAgSUVORK5CYII=" +
                    "^XA\n" +

                    "^MNN^LL400"+
                    "^FO125,70^A0,N,23,23^FD"+custName+"^FS\n" +
                    "^FO125,100^A0,N,20,20^FD"+addrs1+"^FS\n" +
                    "^FO125,130^A0,N,20,20^FD"+addrs2+"^FS\n" +
                    "^FO125,160^A0,N,20,20^FD"+addrs3+"^FS\n" +
                    "^FO450,70^A0,N,20,20^FDNo. Akaun^FS\n" +
                    "^FO540,70^A0,N,20,20^FD:^FS\n" +
                    "^FO550,70^A0,N,20,20^FD"+acctNo+"^FS\n" +
                    "^FO440,100^A0,N,20,20^FDNo Rujukan^FS\n" +
                    "^FO540,100^A0,N,20,20^FD:^FS\n" +
                    "^FO550,100^A0,N,20,20^FD"+orderNo+"^FS\n" +
                    "^FO480,130^A0,N,20,20^FDTarikh^FS\n" +
                    "^FO540,130^A0,N,20,20^FD:^FS\n" +
                    "^FO550,130^A0,N,20,20^FD"+dtStr+"^FS\n" +
                    "\n" +
                    "^FO200,200^BY2\n" +
                    "^A0,N,10,10^B3N,N,45,Y,N\n" +
                    "^FD"+acctNo+"^FS\n" +
                    "\n" +
                    //"^FO125,70^IMR:LOGO2.PNG^FS"+
                    "\n" +
                    "^XZ"
                    //"^XA\n" +
                    //"^IDR:LOGO2.PNG^XZ"
            ;
            */
            zplStr = "^XA^MNN^LL1200^FO35,80^GFA,2900,2900,29, 0000000000000000000000000000000000000000000000000000000000\n" +
                    "0000000000000000000000000000000000000000000000000000000000\n" +
                    "0000000000000000000000000000000000000000000000000000000000\n" +
                    "0000000000000000000000000000000000000000000000000000000000\n" +
                    "0000000000000000000000000000000000000000000000000000000000\n" +
                    "0000000000000000000000000000000000000000000000000000000000\n" +
                    "0000000000000000000000000000000000000000000000000000000000\n" +
                    "0000000000000000000000000000000000000000000000000000000000\n" +
                    "0000000000000000000000000000000000000000000000000000000000\n" +
                    "0000000000000000000000000000000000000000000000000000000000\n" +
                    "0000000000000000000000000000000000000000000000000000000000\n" +
                    "0000000000000000000000000000000000000000000000000000000000\n" +
                    "0003FFFFFFFFFFFF800000000000000000000000000000000000000000\n" +
                    "001FFFFFFFFFFFFFF00000000000000000000000000000000000000000\n" +
                    "007FFFFFFFFFFFFFF80000000000000000000000000000000000000000\n" +
                    "00FFFFFFFFFFFFFFFC0000000000000000000000000000000000000000\n" +
                    "01FFFFFFFFFFFFFFFE0000000000000000000000000000000000000000\n" +
                    "03FFFFFFFFFFFFFFFE0000000000000000000000000000000000000000\n" +
                    "03FFFFFFFFFFFFFFFF0000000000000000000000000000000000000000\n" +
                    "07F8000000000000FF0000000000000000000000000000000000000000\n" +
                    "07F00000000000007F0000000000000000000000000000000000000000\n" +
                    "07F00000000000007F8000000000000000000000000000000000000000\n" +
                    "07F00000001000007F8000000000000000000000000000000000000000\n" +
                    "07F00000002000007F8000000000000000000000000000000000000000\n" +
                    "07F00000006000007F8000000000000000000000000000000000000000\n" +
                    "07F0000000C000007F8000000000000000000000000000000000000000\n" +
                    "07F0000001C000007F8000000000000000000000000000000000000000\n" +
                    "07F00000038000007F8000000000000000000000000000000000000000\n" +
                    "07F00000078000007F8000000000000000000000000000000000000000\n" +
                    "07F000000F0000007F8000000000000000000000000000000000000000\n" +
                    "07F000001F0000007F8000000000000000000000000000000000000000\n" +
                    "07F000003E0000007F8000000000000000000000000000000000000000\n" +
                    "07F000007E0000007F8000000000000000000000000000000000000000\n" +
                    "07F00000FE0000007F8000000000000000000000000000000000000000\n" +
                    "07F00001FC0000007F8000000000000000000000000000000000000000\n" +
                    "07F00003FE0000007F8000000000000000000000000000000000000000\n" +
                    "07F00003FF0000007F8000000000000000000000000000000000000000\n" +
                    "07F00007FFFFC0007F803FFF3FFE7F07F01FE00FF8007F800000000000\n" +
                    "07F0000FFFFFE0007F803FFF3FFE7F07E01FE03FFE007F800000000000\n" +
                    "07F0001FFFFFE0007F803FFF3FFE7F87E03FE07FFF00FF800000000000\n" +
                    "07F0003FFFFFE0007F803FFF3FFC7F87E03FE0FFFF80FF800000000000\n" +
                    "07F0003FFFFFC0007F803FFF3FFC7FC7E07FE1FE3E01FF800000000000\n" +
                    "07F0003FFFFFC0007F8003F03F007FC7E07BF1F81001EFC00000000000\n" +
                    "07F0003FFFFF80007F8003F03F00FFCFE0FBF3F80003EFC00000000000\n" +
                    "07F0000001FF80007F8007F07FF8FFEFC0FBF3F00003EFC00000000000\n" +
                    "07F0000001FF00007F8007F07FF8FFEFC1F1F3F0FF07C7C00000000000\n" +
                    "07F0000001FF00007F8007E07FF8FDFFC1F1F3F0FF07C7C00000000000\n" +
                    "07F0000001FE00007F0007E07FF8FDFFC3F1F3F0FF0FC7C00000000000\n" +
                    "07F8000003FC00007F0007E07FF8FDFFC3E1FBF03F0F87E00000000000\n" +
                    "07FC000003FC0000FF000FE07E01FCFFC7FFFBF83F1FFFE00000000000\n" +
                    "03FFFE0007F801FFFF000FE0FE01F8FF87FFFBF83E1FFFE00000000000\n" +
                    "03FFFF0007F807FFFE000FE0FFF9F87F8FFFF9FC7E3FFFE00000000000\n" +
                    "01FFFF800FF00FFFFE000FC0FFF9F87F8FFFF9FFFE3FFFE00000000000\n" +
                    "01FFFF801FE01FFFFC000FC0FFF9F83F9FC1F8FFFE7F07E00000000000\n" +
                    "00FFFFC01FE01FFFF0000FC0FFF1F83F9F81FC7FFC7E07F00000000000\n" +
                    "003FFFC03FC01FFFE0000FC0FFF1F83FBF80FC0FE0FE03F00000000000\n" +
                    "0000FFC03FC03FFF000000000000000000000000000000000000000000\n" +
                    "00003FC07F803FE0000000000000000000000000000000000000000000\n" +
                    "00001FC07F803FC0000000000000000000000000000000000000000000\n" +
                    "00001FC0FF003FC0000000000000000000000000000000000000000000\n" +
                    "00001FC1FE003F80000000000000000000000000000000000000000000\n" +
                    "00000FC1FE003F80000007E07E03FC03FF0FE03FC03F83F00FE01F8000\n" +
                    "00000FE3FC003F80000007F07E03FC07FF8FC0FFE03F83F01FE01F8000\n" +
                    "00000FFFFC003F8000000FF0FC07FC0FFF8FC1FFF03F83F01FF03F8000\n" +
                    "00000FFFF8003F8000000FF8FC07FE0FFF0FC3FFF83FC3F03FF03F0000\n" +
                    "00000FFFF0003F8000000FF8FC0FFE0FC30FC7FBFC7FC7E03DF03F0000\n" +
                    "000007FFF0003F8000000FF8FC0FBE0FC01FCFE0FC7FE7E07DF03F0000\n" +
                    "000003FFE0003F8000000FFCFC1F3E0FE01FCFC0FC7FE7E07DF03F0000\n" +
                    "000001FFC0003F8000000FFCFC1F3E0FF81F9FC07C7FE7E0F9F83F0000\n" +
                    "000000FF80003F8000000FFEFC3F3E07FE1F9F807C7FF7E0F9F87F0000\n" +
                    "0000000000003F8000001FBEF83E3F03FF1F9F80FC7DF7E1F9F87F0000\n" +
                    "0000000000007F8000001FBFF87E3F00FF9F9F80FC7CFFE1F1F87E0000\n" +
                    "000000000000FF8000001F9FF87E3F003FBF9FC1FCFCFFC3F0F87E0000\n" +
                    "000007FFFFFFFF8000001F9FF8FFFF101FBF9FC3F8FC7FC3FFF87E0000\n" +
                    "00000FFFFFFFFF0000001F8FF8FFFF1C1FBF1FFFF8FC7FC7FFFC7FF800\n" +
                    "00001FFFFFFFFF0000001F8FF9FFFF3F3F3F0FFFF0FC3FC7FFFC7FF000\n" +
                    "00000FFFFFFFFE0000003F07F1FFFFFFFF3F07FFE0FC3FCFFFFCFFF000\n" +
                    "00000FFFFFFFF80000003F07F3F81FBFFE3F03FFC0FC3FCFC0FCFFF000\n" +
                    "000007FFFFFFE00000003F07F3F81F9FFC7F01FF00FC1F9FC0FCFFF000\n" +
                    "00000000000000000000000000000003E0000000000000000000000000\n" +
                    "0000000000000000000000000000000000000000000000000000000000\n" +
                    "0000000000000000000000000000000000000000000000000000000000\n" +
                    "00000007FFFF0000000000000000000000000000000000000000000000\n" +
                    "0000000FFFFF8000000000000000000000000000000000000000000000\n" +
                    "0000000FFFFFC000000000000000000000000000000000000000000000\n" +
                    "0000000FFFFFC000000000000000000000000000000000000000000000\n" +
                    "0000000FFFFFC000000000000000000000000000000000000000000000\n" +
                    "0000000FFFFF8000000000000000000000000000000000000000000000\n" +
                    "00000007FFFF8000000000000000000000000000000000000000000000\n" +
                    "0000000000000000000000000000000000000000000000000000000000\n" +
                    "0000000000000000000000000000000000000000000000000000000000\n" +
                    "0000000000000000000000000000000000000000000000000000000000\n" +
                    "0000000000000000000000000000000000000000000000000000000000\n" +
                    "0000000000000000000000000000000000000000000000000000000000\n" +
                    "0000000000000000000000000000000000000000000000000000000000\n" +
                    "0000000000000000000000000000000000000000000000000000000000\n" +
                    "0000000000000000000000000000000000000000000000000000000000\n" +
                    "0000000000000000000000000000000000000000000000000000000000\n" +
                    "0000000000000000000000000000000000000000000000000000000000\n" +
                    "0000000000000000000000000000000000000000000000000000000000\n" +
                    "^FS\n" +
                    "\n" +
                    "^FO35,220^A0,N,23,23^FDYBhg. Tan Sri/Puan Sri/Datuk/Dato/Datin/Tuan/Puan/Encik/Cik,^FS\n" +
                    "\n" +
                    "^FO135,280^A0,N,28,28^FDKEGAGALAN PENYAMBUNGAN BEKALAN ELEKTRIK^FS\n" +
                    "\n" +
                    "^FB750,10,5,J\n" +
                    "^FO35,340^A0,N,20,20^FDTerlebih dahulu TNB memohon maaf atas gangguan bekalan elektrik di premis YBhg. Tan Sri/Puan\n" +
                    "Sri/Datuk/Dato/Datin/Tuan/Puan/Encik/Cik akibat pemotongan bekalan.^FS\n" +
                    "\n" +
                    "^FB750,10,5,J\n" +
                    "^FO35,420^A0,N,20,20^FDDukacita dimaklumkan, Anggotakerja/wakil TNB telah berkunjung ke premis YBhg. Tan Sri/Puan\n" +
                    "Sri/Datuk/Dato/Datin/ Tuan/Puan/Encik/Cik pada "+dtStr+" untuk membuat penyambungan\n" +
                    " semula. Walau bagaimanapun, penyambungan gagal dilaksanakan akibat berikut:^FS\n" +
                    "\n" +
                    "^FO105,530^A0,N,25,25^FDRiser Meter Berkunci. Maintenance tiada^FS\n" +
                    "\n" +
                    "^FB750,10,5,J\n" +
                    "^FO35,590^A0,N,20,20^FDSekiranya YBhg. Tan Sri/Puan Sri/Datuk/Dato/Datin/Tuan/Puan/Encik/Cik telah membaca notis ini dan pada\n" +
                    " masa sama bekalan masih belum disambung, sila hubungi TNB Careline di talian 15454 untuk bantuan\n" +
                    " selanjutnya.^FS\n" +
                    "\n" +
                    "^FO35,700^A0,N,20,20^FDSekian, terima kasih.^FS\n" +
                    "\n" +
                    "^FB550,10,5,L \n" +
                    "^FO035,780^A0,N,20,20^FDPenyambungan oleh:\\&\n" +
                    "TNB BANGI\n" +
                    "LOT 1, JLN 6C/13,BANDAR BARU BANGI\\& \n" +
                    "43650 BANDAR BARU BANGI\\& \n" +
                    "SELANGOR^FS \n" +
                    "\n" +
                    "^FO35,900^A0,N,20,20^FD---------------------------------------------------------------------------------------^FS\n" +
                    "\n" +
                    "^FO35,930^A0,N,20,20^FDSURAT INI CETAKAN KOMPUTER, TIDAK PERLU TANDA TANGAN^FS\n" +
                    "\n" +
                    "\n" +
                    "\n" +
                    "^FO35,1000^A0,N,20,20^FDNo Akaun:^FS\n" +
                    "^FO125,1000^A0,N,20,20^FD"+acctNo+"^FS\n" +
                    "\n" +
                    "^FO400,1000^A0,N,20,20^FDNo Rujukan:^FS\n" +
                    "^FO500,1000^A0,N,20,20^FD"+orderNo+"^FS\n" +
                    "\n" +
                    "^FO35,1030^A0,N,20,20^FDTarikh Arahan :^FS\n" +
                    "^FO165,1030^A0,N,20,20^FD30.08.2018^FS\n" +
                    "\n" +
                    "^FO400,1030^A0,N,20,20^FDWC ID:^FS\n" +
                    "^FO480,1030^A0,N,20,20^FD96180330^FS\n" +
                    "\n" +
                    "\n" +
                    "^FO35,1100^A0,N,28,28^FDArahan Penyambungan Bekalan^FS\n" +
                    "\n" +
                    "^XZ\n";

            configLabel = zplStr.getBytes();
        } else if (printerLanguage == PrinterLanguage.CPCL) {
            String cpclConfigLabel = "! 0 200 200 406 1\r\n" + "ON-FEED IGNORE\r\n" + "BOX 20 20 380 380 8\r\n" + "T 0 6 137 177 TEST\r\n" + "PRINT\r\n";
            configLabel = cpclConfigLabel.getBytes();
        }
        return configLabel;
    }

    private byte[] getConfigLabel2() {
        PrinterLanguage printerLanguage = printer.getPrinterControlLanguage();

        SimpleDateFormat fmt = new SimpleDateFormat("dd MMM yyyy hh:mm");
        Date dt = new Date();
        String dtStr = fmt.format(dt);

        double dblOB = Double.parseDouble(ob);
        double dblAD = Double.parseDouble(ad);
        double dblRD = Double.parseDouble(rd);
        double total = dblOB + dblAD + dblRD;
        String strTotal = String.valueOf(total);

        byte[] configLabel = null;
        String zplStr = "";
        if (printerLanguage == PrinterLanguage.ZPL) {
            //configLabel = "^XA^FO17,16^GB379,371,8^FS^FT65,255^A0N,135,134^FDTEST^FS^XZ".getBytes();
            /*
            zplStr =
                    //"~DYE:LOGO2.PNG,P,P,3667,,iVBORw0KGg0KICAgDQpJSERSICABPyAgAcEBAyAgICXwLoEgICABc1JHQiCuzhzpICAgBGdBTUEgILGPC/xhBSAgIAZQTFRFICAg////pdmf3SAgIAlwSFlzICAuIiAgLiIBquLdkiAgDQrWSURBVHja7dy7buU2GiBgcjgwUwzMLbcIzHkEl1MYwzzKPIJLF4alIA8wj7CvwiBFyrwCgy22VbCNFhDE/S/U0Y0XBVlsNScziX38xUfi5edPipKI+PKi8tIoZoKTqL5WONShvcG+DvUC57oTcoFjA4ouwZDOLP/ydJAEfc3hGegEhXA1OA0KxXCmo628hGA4DQpThx7OBuFQ/2QGCIOoO6hhQ9A3DhEOUhPsdQv2imDrXODgJMK5dS54NggnrMr6axQdwLF10ljaDuAgGw0KDQokwaAaTRzaTm8Bet1o4vBzbxCaRhOHQwsaYG9bTbyLgwIIxdjshSPBrt7/BdTcKGdBtV2HKk4gJ6zt+gtrGqBsRRQscvEmRnWlXzMcWtDF/rMYdBtaggYjRf1lon8QwbSKEWs7ILStYiR4j7DloMSHe+FdG0qG8xV4J/oLEDoWwG76C1AfulE3vQOYqWpzaHoIxblNPEOd7Q/cETxWtfan3uHmHHS9OI6OWDLyBCN20P1bNgf1JDAciscTPLQyM0LpTAcYM9AOgtry8wGqI3RBUFveQpODXY99BKGvQxrx4L+DCAd46Akz1tgFKAFR9Ycd7E9QjVxZMsjQgvICfBR64KJVXvoNCoQyDCA9QlWB8GnG81u61/1dEUJTJNgTFN8VISiboIF//l6Etsc/GivGiAfxsQZdgg/iVfxQhI46gsae9T0E7f4NCnySoyoXT0d/CH7Sw4cyfFvhix1eynBe4dD9MVyDlAWW4HSDmAv1RShXqOI2CBzglw0K1LtEYw+llyPDSHATN/dQ9wmqSBFyE2oq0G5P+iANCnUs11Mzc7vIt4d2gZLhJjjv4cMNCj3loKIOPfVCt8sL9vAzQegNCoKy2u0YsIdPDAMNChT7cL+HrzcIQcrtuvsevhA08Ju6mUNkHY6UbEBhFeDfxEitB6E9xswtfIbIiRBGD4BDEcphgbMQp8RlA1UgKmduEbry0QahIIihAv4WoCVIA5Sn2G4NCrDzGInTSIYDlS2UYyQIgxFV9MxfZiB8oELILQLHmUIV6hV2AaEsQPgNCqFjTBO3bVGu0CY40GnRfC0PHUOL3bDHHNUW2mOCZmIYig0KFyH8BOYOAqdicD6FrmAZYjCJdHyxDLENCuEbaBiWZoD5XghfYTyBrk8QDrMO4XMVdqxelyA0LGy1DJ+6KsS4DLFMj+K1681k8yEFmyv+5I0hjJ6yDHEEnLFfv3TYWWHMyUCMdBBC4eMBjh3EIfyThR6bqiQ4dBCHdhFg18Ip4E3UeTrxad9pDjBgTGEIGdy2G247V8B88Aa/FCEmgyOWjYNe1mG2tY7Wh36NUQeyIwedpw0KB4SGocPUsSDhc9UNCjHbkuJ9DmqKjpBvOa99DQrCrxN4ylDsN5jNKbANCrwc4H0OYpNYf+OP9F4edgQ9QcvwIQsd9i6H0Hj7E6052CyEpnqELgt1uMHe/lKBalB7OFFLPkPI2QIO1ghNDQqKZ32EMZ8WPmosRIb/pCWZAvx4hNjhstDcoP53DQr+wNAtcKDkOQP9EWKYyUKboMWghysy+Y+W/Q7+Iwb8aRt+jcH+loVqgd72EJhN9PZrFmquaGiSBGHMc6YMHULDsH+zWXh/grPt31egCTBXibOKP072NvPZwgccVrdQjrY3Gfh0hGqwog0KB4DdJGcdIPZn+IoQp0cLhD6GU6sTfDlAOBWXhSNGZ5yZjfBTB9AhDG0IY4jLQXmDE3ayUYwz9EI7nOGA0OAIwhB64VsOKoYwcYVyQSC9cM5BTRCMWOCAcDzDQBAzPvgXNM0YIFS7KlQMfR5CoglfYKbS/TTgsUJeMVXguEL85ukMrV+g+2VkiLlACWICYn8fsTznAoSxEvKnDQqkYeylBDEBMX+MOMXnpGGowf9MCEcs9QzsGOKskOFAQ3wG9gtUMUFXgzTZmxFCfAkqA2OCnlZtGcLIPagCpKkZQhhfo9cg+wwc0owr8ooLJIdh0H4PZYI443IM4S3jB+41NQjRVFSgHjkxZmj7wJ1hCwXm8zTjMgxhTgWQW+QZKprVA1Q4+XKQVB2gQjjR/4DvIIQsv5OBU90dFAuUNzjL33n2fIAzlSemiAjtqGb5rzKkqY/y76CV6Umm1YoDxPc6mv4ofwfQjOoE9QJx6mMHhA6KRqWFkiOE9xzNnAn6bqhAi9NXhj/jSByXqcQOep76qFH5e9H9WoY4mcIDZUhDNi+AZCCN5q8JepOFXRzSuuVTgn0Jjmm19oPyD3TpyEZepDnA38a0UHyHQyMlhnn4K82RQL9jONGlsiOE1OhnrOy04H5PE74C9FSHy8wISRd5friHLuDk+7bksVxIzEGIE+tiBtZJCVqqmrBAviiag5qmrykwdbNQJYixXi8zR2hHGYgpoA0KOGFQy2IU/Hqdgx4jLK/+pfLBBfQCpDnkZsHMFiEtaKzLBDaeaiZBWn5Y5+ouB2k5GmNXt06YuzPEZgix0dLy0e0yBV35DDloeEExnY0sQ1w60rezofI+9EJMIaCE8VDVDQryNeM+D0e61rn9jRk4UjBdIR+j2EMIwbT+H7dVw5d7C9BvLoZ2MR5Dc4JidmGzUOgy0E2UJ8ww2V0v99nUJjLwDQrGj3WOoFMNCu6ylCmtNOM6ygJVqpgdnBn2MEqvl6SWJew9xMwD5h6DXOHSQLYpV2TocRy6QZ3aZgYGFberKM4fIYUzWjSM4VUcXzvYM4S491SDKkGIYPOnOkwzhcOK2QnKBMfslfQdDAnaOhS44umx8eYuKu9WFYYE9TU4nRdxD9DRcoaaxGYS/NegpbUZNQmZufq8W9Cg7BfapJwbcE5QNKCOaXFITFcg/rqxDuFkNX/uSx1Kmpdg635uQY85IcAvdYhzp2uwC1jUTqyX6g0K0A0KCYoWpOUY24b2KsSc1gvTt+GMWeYFqAlq34QQdCAdvSChDuHEVbgC4cRlG0JmjfF5aMMuYN56AbrB5JvtEUKGHrPN9gRHjKZPbWgm/PpzG+oJx3XbhjArgdBo2lBGhHdtCM0Rwve7CxCS3MIelTOcL0HIhuMlaIercHSxvwLN5KK/AvVkCxuXDlDNtrAV6iBlNNegiF8L27WOsPvtKozVj15P1MXqWa8/s7Fajtt9AdWaWaG+DQpVaXucPUBZao8MNyVSauEn2I1XYWF7nCO4+S0uXITWX4SmsD2uI7g5Uy0uQlWAkeCVrW4XoWQYL8PmTkB1FeoE/VV4YVsjwwsbJRk292i6q7BLsFniMcH2htMFNrewLrBRkOYGL2x2Z9jcuLvA5lbgBdYPUm9g9bPdBtbahYlb2Hx9g9/g/wtyt+aYH7k3US/C3jelZbY8TCuGiqEpw2WUbUM3Ld1qrEPL31iCugLNtHTABtQ3OKTlswJU/I3+EzDUodxC2YbqT0DP5Z6FkTbbCLqusof0XzHxl7RwSJCWIvsDpE9bIC8ugE9wDQrN2KrUCdK9PQc4pd3eDKcNCkxtYxOazQnSG0L0B2hL0G/hsIObY5wRrgMSXdc7FQ9DuYOericskJZ9aB0Wj8JvxkKE3E0krwUqLnCCYQt7IVeYGi68afCEdlAsuZw8dAWawuu4GTT1Dt4614jXlVdInXcLb911WC4G3MZru4MpIHTLUvUmA3A72KVvcNI7bmA4QrtA2o+1Zin+tqS/P+suA+OpHOlkPC0ZxzVBUnuoluIheMuk5nXTsdz1wuX+liU34xi4bRRigf0OUjHGbXssQCrGuG3h4pX6tUnpnVtT1x2cGfLG0y2kYjxDaDYpxbIrjJdgT0V6AS4jyuFkaJdcOvtlUnEZ6h0cbjAld3o3TUkNCoav+l6C+GKo2jDFx4FLT/2PYVhmSBm4BKnAQ00TyiZUCfo0HBahvkFTh/YguxJ0CfZpbC/CyBBGBFuFFNrw7x5yV1Pkad8zRlhqpyrwPbHyW6L5DQoWIDSb97hAaoLzn+SEd6wGGBrxiqkd7vFio912hQ4aFUPcpmRpGzt07HvcMr0LKRBtEsQr2Wkokgg/7SPFixBfIIoJb5aiOyjxpgmEj/t+/XyDPUG6vi+es/AjwleDd2vhtVC8IMBw7dffwa/T/QKfRIDjG1Qw/SPAL0YsY6EadQzGE3y6h8QHoJykt/7xXkJZ9TtowwIjQgXQrVDcoHeBTuZ7+JH4Qpt7+i4Q1AcYwwJ7gDAY9ZClIORdPpKgwFtFAxbuhw+KYWTI906e4Xdb+CrwrF94dDvC9xv4QvCVL70f4IczfONx9AjFCXYe939QxgNQrVAeIN/5neCteO7FphwRQvF8BvhuhVwzO8g14126O3VXhUZs6nqB/Q5yo0D4RnDcNIoVYjOj9mh6A9UTxJ14Vl771My68Q6LhWBquAhxaLvjhovwozhCbD3GU1fA1VLsDQoJ4l2+3jJ84V4IkDZ/au5cj6krDHQnMkGoUYKBuiteasNx7pG6q6Rblg0KwxQgINLVZNzaHbm7PvFN0IOJM9274u3ohskNCtBTKeWh+9wnxyHFP4hBX4lRl2H/WYzqKpRXIFTrdAXiDQrvs7gMuzbEe+3jFYh378fetSE9OKC3bUiPIvCmDQo9wnChanqTnqvQeuG2FXpSQ+PFj3S48OyHiR4ScelpEvx8imb5DPLiEy88Pxqjb552r689lQM3EfFzPhpnM9Gd8nhOjbNZnhwyiUYl9ulZJLMQjUOU6Xkpff1JHwPPRmOaNdQ+2SQ4iNrpDOszXQ0Kl8vW1/KUmCvXFRg2LhfcnmTTulzgbvDCVYB53cRXfGGJxPm/UP1pBH/xwWggICAgSUVORK5CYII=" +
                    "^XA\n" +

                    "^MNN^LL400"+
                    "^FO125,70^A0,N,23,23^FD"+custName+"^FS\n" +
                    "^FO125,100^A0,N,20,20^FD"+addrs1+"^FS\n" +
                    "^FO125,130^A0,N,20,20^FD"+addrs2+"^FS\n" +
                    "^FO125,160^A0,N,20,20^FD"+addrs3+"^FS\n" +
                    "^FO450,70^A0,N,20,20^FDNo. Akaun^FS\n" +
                    "^FO540,70^A0,N,20,20^FD:^FS\n" +
                    "^FO550,70^A0,N,20,20^FD"+acctNo+"^FS\n" +
                    "^FO440,100^A0,N,20,20^FDNo Rujukan^FS\n" +
                    "^FO540,100^A0,N,20,20^FD:^FS\n" +
                    "^FO550,100^A0,N,20,20^FD"+orderNo+"^FS\n" +
                    "^FO480,130^A0,N,20,20^FDTarikh^FS\n" +
                    "^FO540,130^A0,N,20,20^FD:^FS\n" +
                    "^FO550,130^A0,N,20,20^FD"+dtStr+"^FS\n" +
                    "\n" +
                    "^FO200,200^BY2\n" +
                    "^A0,N,10,10^B3N,N,45,Y,N\n" +
                    "^FD"+acctNo+"^FS\n" +
                    "\n" +
                    //"^FO125,70^IMR:LOGO2.PNG^FS"+
                    "\n" +
                    "^XZ"
                    //"^XA\n" +
                    //"^IDR:LOGO2.PNG^XZ"
            ;
            */
            zplStr = "^XA\n" +
                    //"~DYE:LOGO.PNG,P,P,3667,,iVBORw0KGg0KICAgDQpJSERSICABPyAgAcEBAyAgICXwLoEgICABc1JHQiCuzhzpICAgBGdBTUEgILGPC/xhBSAgIAZQTFRFICAg////pdmf3SAgIAlwSFlzICAuIiAgLiIBquLdkiAgDQrWSURBVHja7dy7buU2GiBgcjgwUwzMLbcIzHkEl1MYwzzKPIJLF4alIA8wj7CvwiBFyrwCgy22VbCNFhDE/S/U0Y0XBVlsNScziX38xUfi5edPipKI+PKi8tIoZoKTqL5WONShvcG+DvUC57oTcoFjA4ouwZDOLP/ydJAEfc3hGegEhXA1OA0KxXCmo628hGA4DQpThx7OBuFQ/2QGCIOoO6hhQ9A3DhEOUhPsdQv2imDrXODgJMK5dS54NggnrMr6axQdwLF10ljaDuAgGw0KDQokwaAaTRzaTm8Bet1o4vBzbxCaRhOHQwsaYG9bTbyLgwIIxdjshSPBrt7/BdTcKGdBtV2HKk4gJ6zt+gtrGqBsRRQscvEmRnWlXzMcWtDF/rMYdBtaggYjRf1lon8QwbSKEWs7ILStYiR4j7DloMSHe+FdG0qG8xV4J/oLEDoWwG76C1AfulE3vQOYqWpzaHoIxblNPEOd7Q/cETxWtfan3uHmHHS9OI6OWDLyBCN20P1bNgf1JDAciscTPLQyM0LpTAcYM9AOgtry8wGqI3RBUFveQpODXY99BKGvQxrx4L+DCAd46Akz1tgFKAFR9Ycd7E9QjVxZMsjQgvICfBR64KJVXvoNCoQyDCA9QlWB8GnG81u61/1dEUJTJNgTFN8VISiboIF//l6Etsc/GivGiAfxsQZdgg/iVfxQhI46gsae9T0E7f4NCnySoyoXT0d/CH7Sw4cyfFvhix1eynBe4dD9MVyDlAWW4HSDmAv1RShXqOI2CBzglw0K1LtEYw+llyPDSHATN/dQ9wmqSBFyE2oq0G5P+iANCnUs11Mzc7vIt4d2gZLhJjjv4cMNCj3loKIOPfVCt8sL9vAzQegNCoKy2u0YsIdPDAMNChT7cL+HrzcIQcrtuvsevhA08Ju6mUNkHY6UbEBhFeDfxEitB6E9xswtfIbIiRBGD4BDEcphgbMQp8RlA1UgKmduEbry0QahIIihAv4WoCVIA5Sn2G4NCrDzGInTSIYDlS2UYyQIgxFV9MxfZiB8oELILQLHmUIV6hV2AaEsQPgNCqFjTBO3bVGu0CY40GnRfC0PHUOL3bDHHNUW2mOCZmIYig0KFyH8BOYOAqdicD6FrmAZYjCJdHyxDLENCuEbaBiWZoD5XghfYTyBrk8QDrMO4XMVdqxelyA0LGy1DJ+6KsS4DLFMj+K1681k8yEFmyv+5I0hjJ6yDHEEnLFfv3TYWWHMyUCMdBBC4eMBjh3EIfyThR6bqiQ4dBCHdhFg18Ip4E3UeTrxad9pDjBgTGEIGdy2G247V8B88Aa/FCEmgyOWjYNe1mG2tY7Wh36NUQeyIwedpw0KB4SGocPUsSDhc9UNCjHbkuJ9DmqKjpBvOa99DQrCrxN4ylDsN5jNKbANCrwc4H0OYpNYf+OP9F4edgQ9QcvwIQsd9i6H0Hj7E6052CyEpnqELgt1uMHe/lKBalB7OFFLPkPI2QIO1ghNDQqKZ32EMZ8WPmosRIb/pCWZAvx4hNjhstDcoP53DQr+wNAtcKDkOQP9EWKYyUKboMWghysy+Y+W/Q7+Iwb8aRt+jcH+loVqgd72EJhN9PZrFmquaGiSBGHMc6YMHULDsH+zWXh/grPt31egCTBXibOKP072NvPZwgccVrdQjrY3Gfh0hGqwog0KB4DdJGcdIPZn+IoQp0cLhD6GU6sTfDlAOBWXhSNGZ5yZjfBTB9AhDG0IY4jLQXmDE3ayUYwz9EI7nOGA0OAIwhB64VsOKoYwcYVyQSC9cM5BTRCMWOCAcDzDQBAzPvgXNM0YIFS7KlQMfR5CoglfYKbS/TTgsUJeMVXguEL85ukMrV+g+2VkiLlACWICYn8fsTznAoSxEvKnDQqkYeylBDEBMX+MOMXnpGGowf9MCEcs9QzsGOKskOFAQ3wG9gtUMUFXgzTZmxFCfAkqA2OCnlZtGcLIPagCpKkZQhhfo9cg+wwc0owr8ooLJIdh0H4PZYI443IM4S3jB+41NQjRVFSgHjkxZmj7wJ1hCwXm8zTjMgxhTgWQW+QZKprVA1Q4+XKQVB2gQjjR/4DvIIQsv5OBU90dFAuUNzjL33n2fIAzlSemiAjtqGb5rzKkqY/y76CV6Umm1YoDxPc6mv4ofwfQjOoE9QJx6mMHhA6KRqWFkiOE9xzNnAn6bqhAi9NXhj/jSByXqcQOep76qFH5e9H9WoY4mcIDZUhDNi+AZCCN5q8JepOFXRzSuuVTgn0Jjmm19oPyD3TpyEZepDnA38a0UHyHQyMlhnn4K82RQL9jONGlsiOE1OhnrOy04H5PE74C9FSHy8wISRd5friHLuDk+7bksVxIzEGIE+tiBtZJCVqqmrBAviiag5qmrykwdbNQJYixXi8zR2hHGYgpoA0KOGFQy2IU/Hqdgx4jLK/+pfLBBfQCpDnkZsHMFiEtaKzLBDaeaiZBWn5Y5+ouB2k5GmNXt06YuzPEZgix0dLy0e0yBV35DDloeEExnY0sQ1w60rezofI+9EJMIaCE8VDVDQryNeM+D0e61rn9jRk4UjBdIR+j2EMIwbT+H7dVw5d7C9BvLoZ2MR5Dc4JidmGzUOgy0E2UJ8ww2V0v99nUJjLwDQrGj3WOoFMNCu6ylCmtNOM6ygJVqpgdnBn2MEqvl6SWJew9xMwD5h6DXOHSQLYpV2TocRy6QZ3aZgYGFberKM4fIYUzWjSM4VUcXzvYM4S491SDKkGIYPOnOkwzhcOK2QnKBMfslfQdDAnaOhS44umx8eYuKu9WFYYE9TU4nRdxD9DRcoaaxGYS/NegpbUZNQmZufq8W9Cg7BfapJwbcE5QNKCOaXFITFcg/rqxDuFkNX/uSx1Kmpdg635uQY85IcAvdYhzp2uwC1jUTqyX6g0K0A0KCYoWpOUY24b2KsSc1gvTt+GMWeYFqAlq34QQdCAdvSChDuHEVbgC4cRlG0JmjfF5aMMuYN56AbrB5JvtEUKGHrPN9gRHjKZPbWgm/PpzG+oJx3XbhjArgdBo2lBGhHdtCM0Rwve7CxCS3MIelTOcL0HIhuMlaIercHSxvwLN5KK/AvVkCxuXDlDNtrAV6iBlNNegiF8L27WOsPvtKozVj15P1MXqWa8/s7Fajtt9AdWaWaG+DQpVaXucPUBZao8MNyVSauEn2I1XYWF7nCO4+S0uXITWX4SmsD2uI7g5Uy0uQlWAkeCVrW4XoWQYL8PmTkB1FeoE/VV4YVsjwwsbJRk292i6q7BLsFniMcH2htMFNrewLrBRkOYGL2x2Z9jcuLvA5lbgBdYPUm9g9bPdBtbahYlb2Hx9g9/g/wtyt+aYH7k3US/C3jelZbY8TCuGiqEpw2WUbUM3Ld1qrEPL31iCugLNtHTABtQ3OKTlswJU/I3+EzDUodxC2YbqT0DP5Z6FkTbbCLqusof0XzHxl7RwSJCWIvsDpE9bIC8ugE9wDQrN2KrUCdK9PQc4pd3eDKcNCkxtYxOazQnSG0L0B2hL0G/hsIObY5wRrgMSXdc7FQ9DuYOericskJZ9aB0Wj8JvxkKE3E0krwUqLnCCYQt7IVeYGi68afCEdlAsuZw8dAWawuu4GTT1Dt4614jXlVdInXcLb911WC4G3MZru4MpIHTLUvUmA3A72KVvcNI7bmA4QrtA2o+1Zin+tqS/P+suA+OpHOlkPC0ZxzVBUnuoluIheMuk5nXTsdz1wuX+liU34xi4bRRigf0OUjHGbXssQCrGuG3h4pX6tUnpnVtT1x2cGfLG0y2kYjxDaDYpxbIrjJdgT0V6AS4jyuFkaJdcOvtlUnEZ6h0cbjAld3o3TUkNCoav+l6C+GKo2jDFx4FLT/2PYVhmSBm4BKnAQ00TyiZUCfo0HBahvkFTh/YguxJ0CfZpbC/CyBBGBFuFFNrw7x5yV1Pkad8zRlhqpyrwPbHyW6L5DQoWIDSb97hAaoLzn+SEd6wGGBrxiqkd7vFio912hQ4aFUPcpmRpGzt07HvcMr0LKRBtEsQr2Wkokgg/7SPFixBfIIoJb5aiOyjxpgmEj/t+/XyDPUG6vi+es/AjwleDd2vhtVC8IMBw7dffwa/T/QKfRIDjG1Qw/SPAL0YsY6EadQzGE3y6h8QHoJykt/7xXkJZ9TtowwIjQgXQrVDcoHeBTuZ7+JH4Qpt7+i4Q1AcYwwJ7gDAY9ZClIORdPpKgwFtFAxbuhw+KYWTI906e4Xdb+CrwrF94dDvC9xv4QvCVL70f4IczfONx9AjFCXYe939QxgNQrVAeIN/5neCteO7FphwRQvF8BvhuhVwzO8g14126O3VXhUZs6nqB/Q5yo0D4RnDcNIoVYjOj9mh6A9UTxJ14Vl771My68Q6LhWBquAhxaLvjhovwozhCbD3GU1fA1VLsDQoJ4l2+3jJ84V4IkDZ/au5cj6krDHQnMkGoUYKBuiteasNx7pG6q6Rblg0KwxQgINLVZNzaHbm7PvFN0IOJM9274u3ohskNCtBTKeWh+9wnxyHFP4hBX4lRl2H/WYzqKpRXIFTrdAXiDQrvs7gMuzbEe+3jFYh378fetSE9OKC3bUiPIvCmDQo9wnChanqTnqvQeuG2FXpSQ+PFj3S48OyHiR4ScelpEvx8imb5DPLiEy88Pxqjb552r689lQM3EfFzPhpnM9Gd8nhOjbNZnhwyiUYl9ulZJLMQjUOU6Xkpff1JHwPPRmOaNdQ+2SQ4iNrpDOszXQ0Kl8vW1/KUmCvXFRg2LhfcnmTTulzgbvDCVYB53cRXfGGJxPm/UP1pBH/xwWggICAgSUVORK5CYII=" +
                    "^MNN^LL400"+
                    "\n" +
                    "^XZ";

            configLabel = zplStr.getBytes();
        } else if (printerLanguage == PrinterLanguage.CPCL) {
            String cpclConfigLabel = "! 0 200 200 406 1\r\n" + "ON-FEED IGNORE\r\n" + "BOX 20 20 380 380 8\r\n" + "T 0 6 137 177 TEST\r\n" + "PRINT\r\n";
            configLabel = cpclConfigLabel.getBytes();
        }
        return configLabel;
    }

    private void disconnect() {
        try {
            //setStatus("Disconnecting", Color.RED);
            //ctx.showToast("Disconnecting...");
            if (printerConnection != null) {
                printerConnection.close();
            }
            //setStatus("Not Connected", Color.RED);
            //ctx.showToast("Disconnected");
        } catch (ConnectionException e) {
            //setStatus("COMM Error! Disconnected", Color.RED);
            //ctx.showToast("Unable to connect via bluetooth");
        } finally {
            //enableTestButton(true);
        }
    }

    private void sleeper(int ms){
        try {
            Thread.sleep(ms);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private ZebraPrinter connect() {
        //setStatus("Connecting...", Color.YELLOW);
        //ctx.showToast("Connecting to printer...");
        Log.d("DISCON.Debug", "Connecting...");
        printerConnection = null;
        printerConnection = new BluetoothConnection("AC:3F:A4:73:6F:FF");

        try {
            printerConnection.open();
            //setStatus("Connected", Color.GREEN);
            //ctx.showToast("Connected");
            Log.d("DISCON.Debug", "Connected...");
        } catch (ConnectionException e) {
            //setStatus("Comm Error! Disconnecting", Color.RED);
            //ctx.showToast("Unable to connect to printer");
            Log.d("DISCON.Debug", "Comm Error! Disconnecting");
            sleeper(1000);
            disconnect();
        }

        ZebraPrinter printer = null;

        if (printerConnection.isConnected()) {
            try {
                printer = ZebraPrinterFactory.getInstance(printerConnection);
                //setStatus("Determining Printer Language", Color.YELLOW);
                PrinterLanguage pl = printer.getPrinterControlLanguage();
                //setStatus("Printer Language " + pl, Color.BLUE);
            } catch (ConnectionException e) {
                //setStatus("Unknown Printer Language", Color.RED);
                printer = null;
                sleeper(1000);
                disconnect();
            } catch (ZebraPrinterLanguageUnknownException e) {
                //setStatus("Unknown Printer Language", Color.RED);
                printer = null;
                sleeper(1000);
                disconnect();
            }
        }

        return printer;
    }
}
