package onsen.tnbdiscon.com.tnbdiscononsen;

import android.Manifest;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.RectF;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Button;
import android.support.v4.content.ContextCompat;

import java.io.FileNotFoundException;
import java.io.InputStream;

public class MainActivity extends Activity {

    WebView simpleWebView;
    WebAppInterface webAppInterface;
    LocationManager locationManager;
    String target = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        simpleWebView=(WebView) findViewById(R.id.webview);
        simpleWebView.getSettings().setJavaScriptEnabled(true);
        simpleWebView.getSettings().setDomStorageEnabled(true);
        simpleWebView.setWebChromeClient(new WebChromeClient());
        simpleWebView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
        webAppInterface = new WebAppInterface(this, MainActivity.this);
        simpleWebView.addJavascriptInterface(webAppInterface, "AndroidInterface");
        simpleWebView.getSettings().setUseWideViewPort(true);
        simpleWebView.getSettings().setLoadWithOverviewMode(true);
        simpleWebView.loadUrl("file:android_asset/index.html");

        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

        //MyAndroidFirebaseInstanceIdService fcm = new MyAndroidFirebaseInstanceIdService();



        Button clickButton = (Button) findViewById(R.id.button);
        clickButton.setOnClickListener( new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                //simpleWebView.evaluateJavascript("appendText('fazrul izwan')", null);
                //simpleWebView.loadUrl("javascript: appendText(\"fazrul izwanov\")", null);
                //takeImageFromCamera();
                LetterPrinter lPrinter = new LetterPrinter("1", "1", "1", "1", "1", "1", "1", "1", "1", "1", "1", "1", false, 0);
                int status = lPrinter.print();
                //LetterPrinterFailDiscon lPrinter = new LetterPrinterFailDiscon("1", "1", "1", "1", "1", "1", "1", "1", "1", "1", "1", "1", false, 0);
                //int status = lPrinter.print();
                //LetterPrinterReconSuccess lPrinter = new LetterPrinterReconSuccess("1", "1", "1", "1", "1", "1", "1", "1", "1", "1", "1", "1", false, 0, "");
                //int status = lPrinter.print();
                //LetterPrinterReconFail lPrinter = new LetterPrinterReconFail("1", "1", "1", "1", "1", "1", "1", "1", "1", "1", "1", "1", false, 0);
                //int status = lPrinter.print();
                //new LongRunningGetIO("abc123").execute();
                //ZPLTester tester = new ZPLTester();
                //tester.print();
            }
        });
    }

    public void sendMsgToWebview(String callStr){
        simpleWebView.loadUrl(callStr, null);
    }


    @Override
    public void onResume() {
        super.onResume();
        if(target == "discon"){
            target = "";
            //simpleWebView.loadUrl("javascript: goto(\"discon\")", null);
        }
        else if(target == "recon"){
            target = "";
            //simpleWebView.loadUrl("javascript: goto(\"recon\")", null);
        }

    }


    @Override
    public void onBackPressed() {
        simpleWebView.loadUrl("javascript: appendText(\"backbutton\")", null);
    }

    public void takeImageFromCamera() {

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[] { Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE }, 0);
        }

        Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(cameraIntent, 1888);
    }

    public void takeImageFromGallery() {

        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");
        startActivityForResult(photoPickerIntent, 11);
    }


    @Override
    protected void onStart() {
        super.onStart();
        LocalBroadcastManager.getInstance(this).registerReceiver((mMessageReceiver),
                new IntentFilter("FCMData")
        );
    }

    @Override
    protected void onStop() {
        super.onStop();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver);
    }

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            target = intent.getExtras().getString("target");
            System.out.println("********* TARGET received:"+target);
        }
    };


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);

        if(requestCode == 1888){
            if(resultCode == RESULT_OK){
                Bitmap mphoto = (Bitmap) intent.getExtras().get("data");
                String imgBase64 = AppUtil.bitmapToBase64(mphoto);
                System.out.println("test");
                sendMsgToWebview("javascript: appendText(\"image\",\""+imgBase64+"\")");
            }
        }

        if(requestCode == 11){
            if(resultCode == RESULT_OK){
                Uri imageUri = intent.getData();
                InputStream imageStream = null;
                try {
                    imageStream = getContentResolver().openInputStream(imageUri);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
                //BitmapFactory.Options opts = new BitmapFactory.Options();
                //opts.inSampleSize = 4;
                Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);
                Matrix m = new Matrix();
                m.setRectToRect(new RectF(0, 0, selectedImage.getWidth(), selectedImage.getHeight()), new RectF(0, 0, 400, 400), Matrix.ScaleToFit.CENTER);
                sendMsgToWebview("javascript: appendText(\"image\",\""+AppUtil.bitmapToBase64(Bitmap.createBitmap(selectedImage, 0, 0, selectedImage.getWidth(), selectedImage.getHeight(), m, true))+"\")");
            }
        }
    }



}
