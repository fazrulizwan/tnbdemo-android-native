package onsen.tnbdiscon.com.tnbdiscononsen;

import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

/**
 * Created by izwan on 15-Aug-18.
 */



public class MyAndroidFirebaseInstanceIdService extends FirebaseInstanceIdService {

    public MyAndroidFirebaseInstanceIdService(){
        Log.d("MainActivity", "****** FCM created ");
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.d("MainActivity", "******** Refreshed token at creation: " + refreshedToken);
        new LongRunningGetIO(refreshedToken).execute();
    }


    @Override
    public void onTokenRefresh() {
        // Get updated InstanceID token.
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.d("MainActivity", "******** Refreshed token: " + refreshedToken);

        // TODO: Implement this method to send any registration to your app's servers.
        sendRegistrationToServer(refreshedToken);
    }




    private void sendRegistrationToServer(String token){
        System.out.println("************* TOKEN ***********"+token);
        new LongRunningGetIO(token).execute();
    }
}
