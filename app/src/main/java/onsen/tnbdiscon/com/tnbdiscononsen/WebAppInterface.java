package onsen.tnbdiscon.com.tnbdiscononsen;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.webkit.JavascriptInterface;
import android.widget.Toast;

public class WebAppInterface extends Activity{
    Context mContext;
    MainActivity mMain;
    String gpsCoord;
    LocationManager locationManager;
    LocationListener locationListener;
    // Instantiate the interface and set the context
    WebAppInterface(Context c, MainActivity main) {
        mContext = c;
        mMain = main;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_main);

        //locationManager = new LocationManager();
        //locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

    }

    // Show a toast from the web page
    @JavascriptInterface
    public void showToast(String custName, String addrs1, String addrs2, String addrs3, String acctno, String orderno, String ob, String ad, String rd, String img1, String img2, boolean printImg, int imgSize) {
        System.out.println("TESTING>>>>>>>>"+custName+" "+addrs1+" "+addrs2+" "+addrs3+" "+acctno+" "+orderno+" "+ob+" "+ad+" "+rd+"  "+ printImg+" "+imgSize);
        //Toast.makeText(mContext, toast, Toast.LENGTH_SHORT).show();
        LetterPrinter lPrinter = new LetterPrinter(custName, addrs1, addrs2, addrs3, acctno, orderno, ob, ad, rd, img1, img2, "", printImg, imgSize);
        int status = lPrinter.print();
    }

    @JavascriptInterface
    public void printDisconFail(String custName, String addrs1, String addrs2, String addrs3, String acctno, String orderno, String ob, String ad, String rd, String img1, String img2, boolean printImg, int imgSize) {
        System.out.println("TESTING print discon fail>>>>>>>>"+custName+" "+addrs1+" "+addrs2+" "+addrs3+" "+acctno+" "+orderno+" "+ob+" "+ad+" "+rd+"  "+ printImg+" "+imgSize);
        //Toast.makeText(mContext, toast, Toast.LENGTH_SHORT).show();

        LetterPrinterFailDiscon lPrinter = new LetterPrinterFailDiscon(custName, addrs1, addrs2, addrs3, acctno, orderno, ob, ad, rd, img1, img2, "", printImg, imgSize);
        int status = lPrinter.print();
    }


    @JavascriptInterface
    public void printReconFail(String custName, String addrs1, String addrs2, String addrs3, String acctno, String orderno, String ob, String ad, String rd, String img1, String img2, boolean printImg, int imgSize) {
        System.out.println("TESTING print recon fail>>>>>>>>"+custName+" "+addrs1+" "+addrs2+" "+addrs3+" "+acctno+" "+orderno+" "+ob+" "+ad+" "+rd+"  "+ printImg+" "+imgSize);
        //Toast.makeText(mContext, toast, Toast.LENGTH_SHORT).show();

        LetterPrinterReconFail lPrinter = new LetterPrinterReconFail(custName, addrs1, addrs2, addrs3, acctno, orderno, ob, ad, rd, img1, img2, "", printImg, imgSize);
        int status = lPrinter.print();
    }

    @JavascriptInterface
    public void printReconSuccess(String custName, String addrs1, String addrs2, String addrs3, String acctno, String orderno, String ob, String ad, String rd, String img1, String img2, boolean printImg, int imgSize, String MOD) {
        System.out.println("TESTING print recon success>>>>>>>>"+custName+" "+addrs1+" "+addrs2+" "+addrs3+" "+acctno+" "+orderno+" "+ob+" "+ad+" "+rd+"  "+ printImg+" "+imgSize+" "+MOD);
        //Toast.makeText(mContext, toast, Toast.LENGTH_SHORT).show();

        LetterPrinterReconSuccess lPrinter = new LetterPrinterReconSuccess(custName, addrs1, addrs2, addrs3, acctno, orderno, ob, ad, rd, img1, img2, "", printImg, imgSize, MOD);
        int status = lPrinter.print();
    }

    // Show a toast from the web page
    @JavascriptInterface
    public String getGPSLocation() {


        locationListener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                gpsCoord = location.getLongitude()+","+location.getLatitude();
                System.out.println("lsitener gps:"+gpsCoord);
            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {

            }

            @Override
            public void onProviderEnabled(String provider) {

            }

            @Override
            public void onProviderDisabled(String provider) {

            }
        };

       if (ActivityCompat.checkSelfPermission(mMain, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(mMain, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(mMain, new String[] { Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION }, 0);
        }







        mMain.locationManager.requestLocationUpdates("gps", 5000, 0, locationListener);
        System.out.println("to return gps:"+gpsCoord);
        return gpsCoord;
    }

    @JavascriptInterface
    public void getImage(String channel) {
       if(channel.equalsIgnoreCase("camera"))
            mMain.takeImageFromCamera();
       else if(channel.equalsIgnoreCase("gallery"))
            mMain.takeImageFromGallery();
    }

    @JavascriptInterface
    public int getAndroidVersion() {
        return android.os.Build.VERSION.SDK_INT;
    }

    @JavascriptInterface
    public void showAndroidVersion(String versionName) {
        Toast.makeText(mContext, versionName, Toast.LENGTH_SHORT).show();
    }

}
